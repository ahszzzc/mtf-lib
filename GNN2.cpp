template <class AM, class SSM>
struct node* GNN<AM, SSM >::build_graph(double *X)
{
	struct node *Nodes = static_cast<node*>(malloc(params.n_samples*sizeof(struct node)));
	for(int i = 0; i < params.n_samples; i++)
	{
		Nodes[i].nns_inds = static_cast<int*>(malloc(params.k*sizeof(int)));
		check_pointer(Nodes[i].nns_inds, "Couldn't malloc node");
		//   memset(Nodes[i].nns_inds, -1, k*sizeof(int));
		Nodes[i].capacity = params.k;
		Nodes[i].size = 0;
	}

	//  struct indx_dist *dists = malloc(params.n_samples * sizeof(struct indx_dist));
	//  check_pointer(dists, "Couldn't malloc dists");
	struct indx_dist* dists = static_cast<indx_dist*>(malloc((params.k + 1) * sizeof(struct indx_dist)));
	check_pointer(dists, "Couldn't malloc dists");

	int nns_ind;
	//  int *nns_ind = malloc(k*sizeof(int));
	//  check_pointer(nns_ind, "Couldn't malloc nns_ind");
	double *query;
	//  int *query = malloc(X_cols*sizeof(int));
	//  check_pointer(query, "Couldn't malloc query");

	for(int i = 0; i < params.n_samples; i++){
		query = X + (i*X_cols);
		knn_search2(query, dists, X, params.n_samples, X_cols, k + 1);   // index of 1st node is 0
		for(int j = 0; j < params.k; j++){
			nns_ind = dists[j + 1].idx;
			add_node(&Nodes[i], nns_ind);
		}
	}
	free(dists);
	return Nodes;
}
template <class AM, class SSM>
void  GNN<AM, SSM >::add_node(struct node *node_i, int nn)
{
	int size = node_i->size++;
	if(size >= node_i->capacity)
	{
		node_i->nns_inds = static_cast<int*>(realloc(node_i->nns_inds, /*size*2*/ (size + 10)*sizeof(int)));
		node_i->capacity = /*size*2*/ size + 10;
	}
	node_i->nns_inds[size] = nn;
}

template <class AM, class SSM>
struct indx_dist* GNN<AM, SSM >::search_graph(struct node *Nodes, double *Xq, double *X){
	// NNs: graph_size

	double *query = static_cast<double*>(malloc(X_cols*sizeof(double)));
	struct indx_dist *tnn_dists, *gnn_dists, *knns, *visited_nodes;

	tnn_dists = static_cast<indx_dist*>(malloc(/*X_rows*/K * sizeof(struct indx_dist))); //true nn-dists
	int gnns_cap = 1; //NNs*3;
	gnn_dists = static_cast<indx_dist*>(malloc(gnns_cap * sizeof(struct indx_dist))); // graph nn-dists

	int dist_cnt, depth, knns_size;

	for(int j = 0; j < X_cols; j++)
		query[j] = Xq[X_cols + j];

	tnn_dists = static_cast<indx_dist*>(realloc(tnn_dists, /*X_rows*/ K * sizeof(struct indx_dist)));
	knn_search2(query, tnn_dists, X);

	// Graph search time 
	int visited_cap = K * 4;  //avg depth = 4
	int visited = 0;   // number of visited nodes
	visited_nodes = static_cast<indx_dist*>(malloc(visited_cap * sizeof(struct indx_dist)));
	check_pointer(visited_nodes, "Couldn't malloc visited_nodes");

	int rand_node, r;
	double *x_row;
	double parent_dist, dd;

	depth = 0;
	dist_cnt = 0;
	rand_node = my_rand(0, X_rows - 1);
	r = rand_node;
	x_row = &X[r*X_cols]; //X+r*X_cols;
	parent_dist = *am(query, x_row, X_cols);

	visited_nodes[0].idx = r;
	visited_nodes[0].dist = parent_dist;
	visited++;

	while(1)
	{
		if(Nodes[r].size > gnns_cap) //Nodes[r].size != gnns_size)
		{
			gnns_cap = Nodes[r].size;
			gnn_dists = static_cast<indx_dist*>(realloc(gnn_dists, gnns_cap * sizeof(struct indx_dist)));
			check_pointer(gnn_dists, "Couldn't realloc dists");
		}

		knn_search11(query, gnn_dists, X, Nodes[r].size, X_cols, K, Nodes[r].nns_inds);

		//      free(X1); 

		int m = min(K, Nodes[r].size);
		if((visited + m) > visited_cap)
		{
			do {
				visited_cap *= 2;
			} while(visited_cap < (visited + m));
			visited_nodes = static_cast<indx_dist*>(realloc(visited_nodes, visited_cap *sizeof(struct indx_dist)));
			check_pointer(visited_nodes, "Couldn't realloc visited_nodes");
		}
		for(int i = 0; i < m; i++)
		{
			visited_nodes[visited + i].idx = Nodes[r].nns_inds[gnn_dists[i].idx];
			visited_nodes[visited + i].dist = gnn_dists[i].dist;
		}
		visited = visited + m;

		if(parent_dist <= gnn_dists[0].dist)
			break;
		else
			dd = gnn_dists[0].dist;

		r = Nodes[r].nns_inds[gnn_dists[0].idx];
		depth++;
		dist_cnt += Nodes[r].size;
		x_row = &X[r*X_cols]; // X+r*X_cols
		// parent_dist = my_dist(query, x_row, X_cols);
		parent_dist = *am(query, x_row, X_cols);
	}
	//gnns_size = K;
	//gnn_dists = realloc(gnn_dists, gnns_size * sizeof(struct indx_dist));
	//check_pointer(gnn_dists, "Couldn't realloc gnn_dists");

	// Given visited_nodes and their dists, selects the first knns and puts into gnns_dists
	pick_knns(visited_nodes, visited, &gnn_dists, K, &gnns_cap);
	knns = intersect(gnn_dists, tnn_dists, K, &knns_size);


	free(query);
	free(gnn_dists);
	free(tnn_dists);
	return knns;
}

template <class AM, class SSM>
void GNN<AM, SSM >::knn_search2(double *Q, struct indx_dist *dists, double *X){
	// Faster version of knn_search
	// Calculates the distance of query to all data points in X and returns the sorted dist array
	/*  for (i=0; i<rows; i++)
	{
	dists[i].dist = dist_func(Q, X+i*cols, cols);
	dists[i].idx = i;
	}
	mergesort(dists, 0, rows-1);
	*/
	int index, ii, count = 0;
	//int capacity = k;
	for(index = 0; index<params.n_samples; index++){
		double *point = X + index*am_feat_size;

		for(ii = 0; ii<count; ++ii) {
			if(dists[ii].idx == ii) continue; //return false;
		}
		//addPoint(point);
		double dist = *am(Q, point, am_feat_size);
		if(count < params.k)
		{
			dists[count].idx = index;
			dists[count].dist = dist;
			++count;
		} else if(dist < dists[count - 1].dist || (dist == dists[count - 1].dist &&
			index < dists[count - 1].idx)) {
			//         else if (dist < dists[count-1]) {
			dists[count - 1].idx = index;
			dists[count - 1].dist = dist;
		} else {
			continue;   //  return false;
		}

		int i = count - 1;
		while(i >= 1 && (dists[i].dist < dists[i - 1].dist || (dists[i].dist == dists[i - 1].dist &&
			dists[i].idx<dists[i - 1].idx))){
			swap_int(&dists[i].idx, &dists[i - 1].idx);
			swap_double(&dists[i].dist, &dists[i - 1].dist);
			i--;
		}
		//return false;
	}
}
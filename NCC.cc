#include "NCC.h"
#include "imgUtils.h"
#include "histUtils.h"


_MTF_BEGIN_NAMESPACE

NCC::NCC(ParamType *ncc_params) : 
AppearanceModel(ncc_params), params(ncc_params){
	name = "ncc";
	printf("\n");
	printf("Initializing Normalized Cross Correlation appearance model with...\n");
	printf("grad_eps: %e\n", params.grad_eps);
	printf("hess_eps: %e\n", params.hess_eps);

	pix_norm_mult = 1;
	pix_norm_add = 0;
}

void NCC::initialize(){
	if(!is_initialized.similarity){
		init_pix_vals_cntr.resize(n_pix);
		curr_pix_vals_cntr.resize(n_pix);
	}

	init_pix_mean = init_pix_vals.mean();
	init_pix_vals_cntr = (init_pix_vals.array() - init_pix_mean);
	c = init_pix_vals_cntr.norm();

	if(!is_initialized.similarity){
		similarity = 1;
		curr_pix_mean = init_pix_mean;
		curr_pix_vals_cntr = init_pix_vals_cntr;
		b = c;

		is_initialized.similarity = true;
	}
}

void NCC::initializeGrad(){
	if(!is_initialized.grad){
		curr_grad.resize(n_pix);
		init_grad.resize(n_pix);

		init_grad_ncntr.resize(n_pix);
		curr_grad_ncntr.resize(n_pix);

		init_pix_vals_cntr_c.resize(n_pix);
		curr_pix_vals_cntr_b.resize(n_pix);

		init_grad.fill(0);
		curr_grad.fill(0);

		init_grad_ncntr.fill(0);
		curr_grad_ncntr.fill(0);

		init_grad_ncntr_mean = curr_grad_ncntr_mean = 0;

	}
	init_pix_vals_cntr_c = init_pix_vals_cntr.array() / c;

	if(!is_initialized.grad){
		curr_pix_vals_cntr_b = init_pix_vals_cntr_c;
		is_initialized.grad = true;
	}
}

void NCC::update(bool prereq_only){
	curr_pix_mean = curr_pix_vals.mean();
	curr_pix_vals_cntr = (curr_pix_vals.array() - curr_pix_mean);
	a = (init_pix_vals_cntr.array() * curr_pix_vals_cntr.array()).sum();
	b = curr_pix_vals_cntr.norm();	
	bc = b*c;
	b2c = bc*b;
	similarity = a / bc;
}

void NCC::updateInitGrad(){
	for(int i = 0; i < n_pix; i++){
		curr_pix_vals_cntr_b(i) = curr_pix_vals_cntr(i) / b;
		init_grad_ncntr(i) = (curr_pix_vals_cntr_b(i)-similarity*init_pix_vals_cntr_c(i)) / c;
	}
	init_grad_ncntr_mean = init_grad_ncntr.mean();
	init_grad = init_grad_ncntr.array() - init_grad_ncntr_mean;
}
void NCC::updateCurrGrad(){
	for(int i = 0; i < n_pix; i++){
		curr_pix_vals_cntr_b(i) = curr_pix_vals_cntr(i) / b;
		curr_grad_ncntr(i) = (init_pix_vals_cntr_c(i) - similarity*curr_pix_vals_cntr_b(i)) / b;
	}
	curr_grad_ncntr_mean = curr_grad_ncntr.mean();
	curr_grad = curr_grad_ncntr.array() - curr_grad_ncntr_mean;
}

void NCC::cmptInitHessian(MatrixXd &init_hessian, const MatrixXd &init_pix_jacobian){
	int ssm_state_size = init_hessian.rows();
	assert(init_hessian.cols() == ssm_state_size);
	MatrixXd init_pix_jacobian_cntr = (init_pix_jacobian.rowwise() - init_pix_jacobian.colwise().mean()).array() / b;
	init_hessian = -similarity*init_pix_jacobian_cntr.transpose()*init_pix_jacobian_cntr -
		init_pix_jacobian_cntr.transpose()*(curr_pix_vals_cntr_b.transpose()*init_pix_vals_cntr_c + init_pix_vals_cntr_c.transpose()*curr_pix_vals_cntr_b)*init_pix_jacobian_cntr +
		3 * init_pix_jacobian_cntr.transpose()*(init_pix_vals_cntr_c.transpose()*init_pix_vals_cntr_c)*init_pix_jacobian_cntr;
}

void NCC::cmptCurrHessian(MatrixXd &curr_hessian, const MatrixXd &curr_pix_jacobian){
	int ssm_state_size = curr_hessian.rows();
	assert(curr_hessian.cols() == ssm_state_size);
	//curr_hessian.fill(0);
	//double b2 = b*b;
	//double additive_factor = similarity / b2;
	//for(int j = 0; j < n_pix; j++){
	//	//double hess_mean = 0;
	//	hess = (3 * curr_pix_vals_cntr_b(j)*curr_pix_vals_cntr_b -
	//		curr_pix_vals_cntr_b(j)*init_pix_vals_cntr_c -
	//		init_pix_vals_cntr_c(j)*curr_pix_vals_cntr_b).array() / b2;
	//	hess(j) += additive_factor;
	//	hess.array() -= hess.mean();
	//	curr_hessian += curr_pix_jacobian.transpose() * hess * curr_pix_jacobian.row(j);
	//}
	MatrixXd curr_pix_jacobian_cntr = (curr_pix_jacobian.rowwise() - curr_pix_jacobian.colwise().mean()).array()/b;
	curr_hessian = -similarity*curr_pix_jacobian_cntr.transpose()*curr_pix_jacobian_cntr -
		curr_pix_jacobian_cntr.transpose()*(curr_pix_vals_cntr_b.transpose()*init_pix_vals_cntr_c + init_pix_vals_cntr_c.transpose()*curr_pix_vals_cntr_b)*curr_pix_jacobian_cntr +
		3 * curr_pix_jacobian_cntr.transpose()*(curr_pix_vals_cntr_b.transpose()*curr_pix_vals_cntr_b)*curr_pix_jacobian_cntr;


}


void NCC::cmptInitHessian(MatrixXd &init_hessian, const MatrixXd &init_pix_jacobian,
	const MatrixXd &init_pix_hessian){
	int ssm_state_size = init_hessian.rows();
	assert(init_hessian.cols() == ssm_state_size);
	assert(init_pix_hessian.rows() == ssm_state_size * ssm_state_size);

	MatrixXd init_pix_jacobian_cntr = (init_pix_jacobian.rowwise() - init_pix_jacobian.colwise().mean()).array() / b;
	init_hessian = -similarity*init_pix_jacobian_cntr.transpose()*init_pix_jacobian_cntr -
		init_pix_jacobian_cntr.transpose()*(curr_pix_vals_cntr_b.transpose()*init_pix_vals_cntr_c + init_pix_vals_cntr_c.transpose()*curr_pix_vals_cntr_b)*init_pix_jacobian_cntr +
		3 * init_pix_jacobian_cntr.transpose()*(init_pix_vals_cntr_c.transpose()*init_pix_vals_cntr_c)*init_pix_jacobian_cntr;
	for(int j = 0; j < n_pix; j++){
		init_hessian += Map<MatrixXd>((double*)init_pix_hessian.col(j).data(), ssm_state_size, ssm_state_size) * init_grad(j);;
	}

}

void NCC::cmptCurrHessian(MatrixXd &curr_hessian, const MatrixXd &curr_pix_jacobian,
	const MatrixXd &curr_pix_hessian){
	int ssm_state_size = curr_hessian.rows();
	assert(curr_hessian.cols() == ssm_state_size);
	assert(curr_pix_hessian.rows() == ssm_state_size * ssm_state_size);

	MatrixXd curr_pix_jacobian_cntr = (curr_pix_jacobian.rowwise() - curr_pix_jacobian.colwise().mean()).array() / b;
	curr_hessian = -similarity*curr_pix_jacobian_cntr.transpose()*curr_pix_jacobian_cntr -
		curr_pix_jacobian_cntr.transpose()*(curr_pix_vals_cntr_b.transpose()*init_pix_vals_cntr_c + init_pix_vals_cntr_c.transpose()*curr_pix_vals_cntr_b)*curr_pix_jacobian_cntr +
		3 * curr_pix_jacobian_cntr.transpose()*(curr_pix_vals_cntr_b.transpose()*curr_pix_vals_cntr_b)*curr_pix_jacobian_cntr;
	for(int j = 0; j < n_pix; j++){
		curr_hessian += Map<MatrixXd>((double*)curr_pix_hessian.col(j).data(), ssm_state_size, ssm_state_size) * curr_grad(j);;
	}
}

void NCC::cmptSelfHessian(MatrixXd &self_hessian, const MatrixXd &curr_pix_jacobian){
	assert(self_hessian.cols() == self_hessian.rows());

	MatrixXd curr_pix_jacobian_cntr = (curr_pix_jacobian.rowwise() - curr_pix_jacobian.colwise().mean()).array() / b;
	self_hessian = -curr_pix_jacobian_cntr.transpose()*curr_pix_jacobian_cntr + curr_pix_jacobian_cntr.transpose()*(curr_pix_vals_cntr_b.transpose()*curr_pix_vals_cntr_b)*curr_pix_jacobian_cntr;
}

/*Support for FLANN library*/



void NCC::updateDistFeat(double* feat_addr){
	curr_pix_mean = curr_pix_vals.mean();
	double pix_norm = 0;
	for(size_t pix = 0; pix < n_pix; pix++) {
		*feat_addr = curr_pix_vals(pix) - curr_pix_mean;
		pix_norm += *feat_addr * *feat_addr;
		feat_addr++;
	}
	*feat_addr = sqrt(pix_norm);
}


double NCC::operator()(const double* a, const double* b, 
	size_t size, double worst_dist) const {
	double result = double();
	double num = 0;
	size_t n_pix = size - 1;
	const double* last = a + n_pix;
	const double* lastgroup = last - 3;

	/* Process 4 items with each loop for efficiency. */
	while(a < lastgroup) {

		num += a[0] * b[0] + a[1] * b[1] + a[2] * b[2] + a[3] * b[3];
		a += 4;
		b += 4;
	}
	/* Process last 0-3 pixels.  Not needed for standard vector lengths. */
	while(a < last) {

		num += *a * *b;
		a++;
		b++;
	}

	result = -num / *a * *b;
	return result;
}

_MTF_END_NAMESPACE


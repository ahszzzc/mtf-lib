Implementation of a visual tracking framework that utilizes a modular decomposition of registration based trackers. 
Each tracker within this framework comprises the following 3 modules:
1. Search Method: ESM, IC, IA, FC, FA, NN, PF or RKLT
2. Appearance Model: SSD, ZNCC, SCV, NCC, MI, CCRE or SSIM
3. State Space Model: Homography (8dof), Affine (6 dof), Similarity(4 dof), Isometery (3 dof) or pure Translation (2 dof)

Installation:
-------------
Prerequisites:

1. Eigen should be present in the include path.
2. OpenCV should be installed.
3. FLANN library (http://www.cs.ubc.ca/research/flann/) and its dependency HDF5 (https://www.hdfgroup.org/HDF5/release/obtain5.html) should be installed for the NN tracker
4. Intel TBB should be installed
5. ViSP library (https://visp.inria.fr/) should be installed if its template tracker module (https://visp.inria.fr/template-tracking/) is to be used, otherwise compile with vp=0 to disable it.
6. Xvision should be installed if it is enabled during compilation (see below).

Following make commands are available:

1. **make mtf** : compiles the shared library (.so file)
1. **make install** : compiles the shared library (.so file) and copies it to /usr/lib; this needs sudo permission
2. **make mtfr**: compiles the file runMTF.cc to create an executable that uses this library to track objects
3. **make** or **make mtfi** : all of the above
	- specifying **xv=1** with this (or the previous) command will enable Xvision trackers and pipeline too (disabled by default).
	- specifying **vp=0** will disable VisP tracker (enabled by default).
4. **make run**: creates (if needed) and runs the above executable (doesn't compile or install the library)
5. **make clean** : removes all the .o files and the .so file created during compilation from the MTF folder
6. **make mtfc**: also removes the executable

Setting Parameters:
-------------------

Refer to the ReadMe.txt file in the Tools sub folder for instructions on how to specify parameters for the tracking task.
#include "NN.h"
#include <time.h>
#include <ctime> 
#include <flann/io/hdf5.h>
#include "miscUtils.h"



_MTF_BEGIN_NAMESPACE


template <class AM, class SSM>
NN<AM, SSM >::NN(ParamType *nn_params, 
	AMParams *am_params, SSMParams *ssm_params) : 
	SearchMethod<AM, SSM>(am_params, ssm_params),
	params(nn_params){

	printf("\n");
	printf("initializing Nearest Neighbor tracker with:\n");
	printf("max_iters: %d\n", params.max_iters);
	printf("n_samples: %d\n", params.n_samples);
	printf("upd_thresh: %f\n", params.upd_thresh);

	if(params.pix_sigma <= 0){
		if(params.ssm_sigma.size() < ssm->getStateSize()){
			stringstream err_msg;
			err_msg << "NN :: SSM sigma has too gew values " << params.ssm_sigma.size() << "\n";
			throw std::invalid_argument(err_msg.str());
		}
		printf("ssm_sigma: \n");
		for(int state_id = 0; state_id < ssm->getStateSize(); state_id++){
			printf("%f ", params.ssm_sigma[state_id]);
		}
		printf("\n");
	}else{
		printf("pix_sigma: %f\n", params.pix_sigma);
	}

	printf("corner_sigma_d: %f\n", params.corner_sigma_d);
	printf("corner_sigma_t: %f\n", params.corner_sigma_t);

	printf("index_type: %d (%s)\n", params.index_type,
		NNIndex::getIndexType(params.index_type));
	printf("n_checks: %d\n", params.n_checks);
	printf("additive_update: %d\n", params.additive_update);
	printf("direct_samples: %d\n", params.direct_samples);
	printf("ssm_sigma_prec: %f\n", params.ssm_sigma_prec);
	printf("debug_mode: %d\n", params.debug_mode);

	printf("appearance model: %s\n", am->name.c_str());
	printf("state space model: %s\n", ssm->name.c_str());
	printf("\n");

	name = "nn";
	log_fname = "log/mtf_nn_log.txt";
	time_fname = "log/mtf_nn_times.txt";
	frame_id = 0;

	ssm_state_size = ssm->getStateSize();
	am_dist_size = am->getDistFeatSize();

	state_sigma.resize(ssm_state_size);
	state_sigma = VectorXdM(params.ssm_sigma.data(), ssm_state_size);

	utils::printMatrix(state_sigma, "state_sigma");

	printf("ssm_state_size: %d\n", ssm_state_size);
	printf("am_dist_size: %d\n", am_dist_size);

	//flann_query = new flannMatT(const_cast<double*>(eig_query.data()), 
	//	eig_query.rows(), eig_query.cols());

	//flann_result = new flannResultT(const_cast<int*>(eig_result.data()),
	//	eig_result.rows(), eig_result.cols());
	//flann_dists = new flannMatT(const_cast<double *>(eig_dists.data()),
	//	eig_dists.rows(), eig_dists.cols());

	eig_dataset.resize(params.n_samples, am_dist_size);
	//eig_query.resize(am_dist_size);
	eig_result.resize(1);
	eig_dists.resize(1);

	flann_dataset = new flannMatT(const_cast<double *>(eig_dataset.data()),
		eig_dataset.rows(), eig_dataset.cols());

	ssm_update.resize(ssm_state_size);
	ssm_perturbations.resize(ssm_state_size, params.n_samples);
	inv_state_update.resize(ssm_state_size);

	//pix_grad_identity.resize(am->getPixCount(), Eigen::NoChange);
	//pix_grad_identity.fill(1);
	//ssm_grad_norm.resize(am->getPixCount(), ssm_state_size);
	//ssm_grad_norm_mean.resize(ssm_state_size);
}

template <class AM, class SSM>
void NN<AM, SSM >::initialize(const cv::Mat &corners){
	start_timer();

	am->clearInitStatus();
	ssm->clearInitStatus();
	
	ssm->initialize(corners);

	if(params.pix_sigma <= 0){
		ssm->initializeSampler(state_sigma);
	} else{
		ssm->initializeSampler(params.pix_sigma);
	}

	am->initializePixVals(ssm->getPts());
	am->initializeDistFeat();

	//utils::printMatrix(ssm->getCorners(), "init_corners original");
	//utils::printMatrix(ssm->getCorners(), "init_corners after");
	//utils::printScalarToFile("initializing NN...", " ", log_fname, "%s", "w");

	//baseGeneratorType generator(rand());
	//dustributionType gaussian_dist_ssm[ssm_state_size];
	//randomGeneratorType* rand_gen_ssm[ssm_state_size];
	//dustributionType gaussian_dist_d(0, params.corner_sigma_d);
	//dustributionType gaussian_dist_t(0, params.corner_sigma_t);
	//randomGeneratorType rand_gen_d(generator, gaussian_dist_d);
	//randomGeneratorType rand_gen_t(generator, gaussian_dist_t);

	//if(params.direct_samples){
	//	utils::printScalarToFile("ssm_grad", nullptr, "log/ssm_grad.txt", "%s", "a");
	//	Matrix2Xd pix_ssm_grad;
	//	pix_ssm_grad.resize(Eigen::NoChange, ssm_state_size);
	//	for(int pix_id = 0; pix_id < am->getPixCount(); pix_id++){
	//		if(params.additive_update){
	//			ssm->getCurrPixGrad(pix_ssm_grad, pix_id);
	//		} else{
	//			ssm->getInitPixGrad(pix_ssm_grad, pix_id);
	//		}
	//		ssm_grad_norm.row(pix_id) = pix_ssm_grad.colwise().norm();
	//	}
	//	utils::printMatrixToFile(ssm_grad_norm, "ssm_grad_norm", "log/ssm_grad_norm.txt");
	//	ssm_grad_norm_mean = ssm_grad_norm.colwise().mean();
	//	for(int i = 0; i < ssm_state_size; i++){
	//		ssm_sigma(i) = params.corner_sigma_d / ssm_grad_norm_mean(i);
	//	}
	//	utils::printMatrix(ssm_grad_norm_mean, "ssm_jacobian_mean");
	//	utils::printMatrix(ssm_sigma, "ssm_sigma");
	//	for(int i = 0; i < ssm_state_size; i++){
	//		gaussian_dist_ssm[i] = dustributionType(0, ssm_sigma(i));
	//		rand_gen_ssm[i] = new randomGeneratorType(generator, gaussian_dist_ssm[i]);
	//	}
	//}


	//Matrix24d rand_d;
	//Vector2d rand_t;
	//Matrix24d disturbed_corners;

	VectorXd state_update(ssm_state_size);
	printf("building feature dataset...\n");
	for(int sample_id = 0; sample_id < params.n_samples; ++sample_id){

		//utils::printMatrix(ssm->getCorners(), "Corners before");

		ssm->generatePerturbation(state_update);
		//utils::printMatrix(state_update, "state_update");

		if(params.additive_update){
			inv_state_update = -state_update;
			ssm->additiveUpdate(inv_state_update);
		} else{
			ssm->invertState(inv_state_update, state_update);
			ssm->compositionalUpdate(inv_state_update);
		}
		//utils::printMatrix(inv_state_update, "inv_state_update");

		am->updatePixVals(ssm->getPts());
		am->updateDistFeat(eig_dataset.row(sample_id).data());

		ssm_perturbations.col(sample_id) = state_update;

		// reset SSM to previous state
		if(params.additive_update){
			ssm->additiveUpdate(state_update);
		} else{
			ssm->compositionalUpdate(state_update);
		}
		//utils::printMatrix(ssm->getCorners(), "Corners after");


	}

	printf("building flann index...\n");
	flann_index = new flannIdxT(*flann_dataset, NNIndex::getIndexParams(params.index_type));
	//flann_index = new flannIdxT(*flann_dataset, flann::KDTreeIndexParams(params.n_trees));
	flann_index->buildIndex();

	if(params.debug_mode){
		flann::save_to_file<double>(*flann_dataset, "log/flann_log.hdf5", "flann_dataset");
	}
	ssm->getCorners(cv_corners);

	end_timer();
	write_interval(time_fname, "w");
	}

template <class AM, class SSM>
void NN<AM, SSM >::update(){
	++frame_id;
	write_frame_id(frame_id);

	am->setFirstIter();
	for(int i = 0; i < params.max_iters; i++){
		init_timer();

		am->updatePixVals(ssm->getPts());
		record_event("am->updatePixVals");

		am->updateDistFeat();
		record_event("am->updateDistFeat");

		flannMatT flann_query(const_cast<double*>(am->getDistFeat()), 1, am_dist_size);
		record_event("am->updateDistFeat");

		flannResultT flann_result(&best_idx, 1, 1);
		flannMatT flann_dists(&best_dist, 1, 1);
		record_event("flann_dists/result");

		flann_index->knnSearch(flann_query, flann_result, flann_dists, 1, flann::SearchParams(params.n_checks));
		ssm_update = ssm_perturbations.col(best_idx);
		record_event("flann_index->knnSearch");

		//if(params.debug_mode){
		//	printf("The nearest neighbor is at index %d with distance %f\n", best_idx, best_dist);
		//	//flann::save_to_file(flann_result, "log/flann_log.hdf5", "flann_result");
		//	//flann::save_to_file(flann_dists, "log/flann_log.hdf5", "flann_dists");
		//	//utils::printMatrix(ssm_update, "ssm_update");
		//}

		prev_corners = ssm->getCorners();

		if(params.additive_update){
			ssm->additiveUpdate(ssm_update);
			record_event("ssm->additiveUpdate");
		} else{
			ssm->compositionalUpdate(ssm_update);
			record_event("ssm->compositionalUpdate");
		}

		double update_norm = (prev_corners - ssm->getCorners()).squaredNorm();
		record_event("update_norm");

		write_data(time_fname);

		if(update_norm < params.upd_thresh){
			if(params.debug_mode){
				printf("n_iters: %d\n", i + 1);
			}
			break;
		}

		am->clearFirstIter();
	}
	ssm->getCorners(cv_corners);
}


//template <class AM, class SSM>
//double NN<AM, SSM >::getCornerChangeNorm(const VectorXd &state_update){
//
//	utils::printMatrix(ssm->getCorners(), "corners before");
//	utils::printMatrix(state_update, "state_update");
//	ssm->compositionalUpdate(state_update);
//
//	utils::printMatrix(ssm->getCorners(), "corners middle");
//	//utils::printMatrix(ssm->getCorners(), "init corners");
//
//	Matrix24d corner_change = ssm->getCorners() - ssm->getCorners();
//	utils::printMatrix(corner_change, "corner_change");
//	double corner_change_norm = corner_change.lpNorm<1>() / 8;
//	ssm->invertState(inv_state_update, state_update);
//	ssm->compositionalUpdate(inv_state_update);
//	//utils::printMatrix(ssm->getCorners(), "corners after");
//	return corner_change_norm;
//}
//
//template <class AM, class SSM>
//double NN<AM, SSM >::findSSMSigma(int param_id){
//	double param_std = params.corner_sigma_d;
//	VectorXd state_update(ssm->getStateSize());
//	Matrix3d inv_warp;
//	state_update.fill(0);
//	Matrix24d corner_change;
//	while(true){
//		state_update(param_id) = param_std;
//
//		double corner_change_norm = getCornerChangeNorm(state_update);
//		double perturbation_ratio = corner_change_norm / params.corner_sigma_d;
//
//		if(perturbation_ratio > params.ssm_sigma_prec){
//			param_std /= params.ssm_sigma_prec;
//		} else if(perturbation_ratio < 1 / params.ssm_sigma_prec){
//			param_std *= params.ssm_sigma_prec;
//		} else{
//			return param_std;
//		}
//	}
//}
//
//template <class AM, class SSM>
//double NN<AM, SSM >::findSSMSigmaGrad(int param_id){
//	double param_std = params.corner_sigma_d;
//	VectorXd state_update(ssm->getStateSize());
//	state_update.fill(0);
//	state_update(param_id) = param_std + NN_CORNER_GRAD_EPS;
//	double corner_change_inc = getCornerChangeNorm(state_update);
//	state_update(param_id) = param_std - NN_CORNER_GRAD_EPS;
//	double corner_change_dec = getCornerChangeNorm(state_update);
//	double corner_change_grad = (corner_change_inc - corner_change_dec) / (2 * NN_CORNER_GRAD_EPS);
//	double ssm_sigma = params.corner_sigma_d / corner_change_grad;
//
//	utils::printScalar(param_id, "\nparam_id", "%d");
//	utils::printScalar(corner_change_inc, "corner_change_inc");
//	utils::printScalar(corner_change_dec, "corner_change_dec");
//	utils::printScalar(corner_change_grad, "corner_change_grad");
//	utils::printScalar(ssm_sigma, "ssm_sigma");
//
//	return ssm_sigma;
//}


const char* NNIndex::getIndexType(IdxType index_type){
	switch(index_type){
	case IdxType::KDTree:
		return "KDTree";
	case IdxType::HierarchicalClustering:
		return "HierarchicalClustering";
	case IdxType::KMeans:
		return "KMeans";
	case IdxType::Composite:
		return "Composite";
	case IdxType::Linear:
		return "Linear";
	case IdxType::KDTreeSingle:
		return "KDTreeSingle";
	case IdxType::KDTreeCuda3d:
		return "KDTreeCuda3d";
	case IdxType::Autotuned:
		return "Autotuned";
	case IdxType::SavedIndex:
		return "Saved";
	default:
		throw std::invalid_argument("NNIndex::Invalid index type provided");
	}
}

void NNIndex::processStringParam(char* &str_out, const char* param){
	if(!strcmp(param, "#")){
		// use default value
		return;
	}
	str_out = new char[strlen(param) + 1];
	strcpy(str_out, param);
}

void NNIndex::processAgrument(char *arg_name, char *arg_val){
	if(!strcmp(arg_name, "kdt_trees")){
		kdt_trees = atoi(arg_val);
	} else if(!strcmp(arg_name, "km_branching")){
		km_branching = atoi(arg_val);
	} else if(!strcmp(arg_name, "km_iterations")){
		km_iterations = atoi(arg_val);
	} else if(!strcmp(arg_name, "km_centers_init")){
		km_centers_init = static_cast<flann::flann_centers_init_t>(atoi(arg_val));
	} else if(!strcmp(arg_name, "km_cb_index")){
		km_cb_index = atof(arg_val);
	} else if(!strcmp(arg_name, "kdts_leaf_max_size")){
		kdts_leaf_max_size = atoi(arg_val);
	} else if(!strcmp(arg_name, "kdtc_leaf_max_size")){
		kdtc_leaf_max_size = atoi(arg_val);
	} else if(!strcmp(arg_name, "branching")){
		hc_branching = atoi(arg_val);
	} else if(!strcmp(arg_name, "centers_init")){
		hc_centers_init = static_cast<flann::flann_centers_init_t>(atoi(arg_val));
	} else if(!strcmp(arg_name, "trees")){
		hc_trees = arg_val[0];
	} else if(!strcmp(arg_name, "leaf_max_size")){
		hc_leaf_max_size = arg_val[0];
	} else if(!strcmp(arg_name, "auto_target_precision")){
		auto_target_precision = atof(arg_val);
	} else if(!strcmp(arg_name, "auto_build_weight")){
		auto_build_weight = atof(arg_val);
	} else if(!strcmp(arg_name, "auto_memory_weight")){
		auto_memory_weight = atof(arg_val);
	} else if(!strcmp(arg_name, "auto_sample_fraction")){
		auto_sample_fraction = atof(arg_val);
	} else if(!strcmp(arg_name, "saved_idx_fname")){
		processStringParam(saved_idx_fname, arg_val);
	}
}

const flann::IndexParams NNIndex::getIndexParams(IdxType index_type,
	char* param_fname){

	FILE *fid = fopen(param_fname, "r");
	if(!fid){
		printf("\nNN index param file: %s not found. Using default parameters...\n", param_fname);
	} else{
		char arg_pair[500], arg_name[500], arg_val[500];
		while(!feof(fid)){
			fgets(arg_pair, 500, fid);
			strtok(arg_pair, "\n");
			strtok(arg_pair, "\r");
			if(arg_pair[0] == '#'){
				continue;
			}
			if(strlen(arg_pair) > 1){
				sscanf(arg_pair, "%s%s", arg_name, arg_val);
				processAgrument(arg_name, arg_val);
			}
		}
	}
	fclose(fid);
	switch(index_type){
	case IdxType::Linear:
		printf("Using Linear index\n");
		return flann::LinearIndexParams();
	case IdxType::KDTree:
		printf("Using KD Tree index with:\n");
		printf("n_trees: %d\n", kdt_trees);
		return flann::KDTreeIndexParams(kdt_trees);
	case IdxType::KMeans:
		if(!(km_centers_init == flann::FLANN_CENTERS_RANDOM ||
			km_centers_init == flann::FLANN_CENTERS_GONZALES ||
			km_centers_init == flann::FLANN_CENTERS_KMEANSPP)){
			printf("Invalid method provided for selecting initial centers: %d. Using random centers...\n", km_centers_init);
			km_centers_init = flann::FLANN_CENTERS_RANDOM;
		}
		printf("Using KMeans index with:\n");
		printf("branching: %d\n", km_branching);
		printf("iterations: %d\n", km_iterations);
		printf("centers_init: %d\n", km_centers_init);
		printf("cb_index: %f\n", km_cb_index);
		return flann::KMeansIndexParams(km_branching, km_iterations,
			km_centers_init, km_cb_index);
	case IdxType::Composite:
		if(!(km_centers_init == flann::FLANN_CENTERS_RANDOM ||
			km_centers_init == flann::FLANN_CENTERS_GONZALES ||
			km_centers_init == flann::FLANN_CENTERS_KMEANSPP)){
			printf("Invalid method provided for selecting initial centers: %d. Using random centers...\n", km_centers_init);
			km_centers_init = flann::FLANN_CENTERS_RANDOM;
		}
		printf("Using Composite index with:\n");
		printf("n_trees: %d\n", kdt_trees);
		printf("branching: %d\n", km_branching);
		printf("iterations: %d\n", km_iterations);
		printf("centers_init: %d\n", km_centers_init);
		printf("cb_index: %f\n", km_cb_index);
		return flann::CompositeIndexParams(kdt_trees, km_branching, km_iterations,
			km_centers_init, km_cb_index);
	case IdxType::HierarchicalClustering:
		if(!(hc_centers_init == flann::FLANN_CENTERS_RANDOM ||
			hc_centers_init == flann::FLANN_CENTERS_GONZALES ||
			hc_centers_init == flann::FLANN_CENTERS_KMEANSPP)){
			printf("Invalid method provided for selecting initial centers: %d. Using random centers...\n", km_centers_init);
			hc_centers_init = flann::FLANN_CENTERS_RANDOM;
		}
		printf("Using Hierarchical Clustering index with:\n");
		printf("branching: %d\n", hc_branching);
		printf("centers_init: %d\n", hc_centers_init);
		printf("trees: %d\n", hc_trees);
		printf("leaf_max_size: %d\n", hc_leaf_max_size);
		return flann::HierarchicalClusteringIndexParams(hc_branching,
			hc_centers_init, hc_trees, hc_leaf_max_size);
	case IdxType::KDTreeSingle:
		printf("Using KDTreeSingle index with:\n");
		printf("leaf_max_size: %d\n", kdtc_leaf_max_size);
		return flann::KDTreeSingleIndexParams(kdts_leaf_max_size);
		//case KDTreeCuda3d:
		//	printf("Using KDTreeCuda3d index with:\n");
		//	printf("leaf_max_size: %d\n", kdtc_leaf_max_size);
		//	return flann::KDTreeCuda3dIndexParams(kdtc_leaf_max_size);
	case IdxType::Autotuned:
		printf("Using Autotuned index with:\n");
		printf("target_precision: %f\n", auto_target_precision);
		printf("build_weight: %f\n", auto_build_weight);
		printf("memory_weight: %f\n", auto_memory_weight);
		printf("sample_fraction: %f\n", auto_sample_fraction);
		return flann::AutotunedIndexParams(auto_target_precision, auto_build_weight,
			auto_memory_weight, auto_sample_fraction);
	case IdxType::SavedIndex:
		printf("Loading SavedIndex from: %s\n", saved_idx_fname);
		return flann::SavedIndexParams(saved_idx_fname);
	default: printf("Invalid index type specified: %d. Using KD Tree index by default...\n", index_type);
		return flann::KDTreeIndexParams(kdt_trees);
	}
}

NNParams::NNParams(int _max_iters, int _n_samples, double _upd_thresh,
	const vector<double> &_ssm_sigma,
	double _corner_sigma_d, double _corner_sigma_t,
	double _pix_sigma,
	NNIndex::IdxType _index_type, int _n_checks,
	bool _additive_update, bool _direct_samples,
	double _ssm_sigma_prec, bool _debug_mode){
	max_iters = _max_iters;
	n_samples = _n_samples;
	upd_thresh = _upd_thresh;
	ssm_sigma = _ssm_sigma;
	corner_sigma_d = _corner_sigma_d;
	corner_sigma_t = _corner_sigma_t;
	pix_sigma = _pix_sigma;
	index_type = _index_type;
	n_checks = _n_checks;
	additive_update = _additive_update;
	direct_samples = _direct_samples;
	ssm_sigma_prec = _ssm_sigma_prec;
	debug_mode = _debug_mode;
}

NNParams::NNParams(NNParams *params) :
max_iters(NN_MAX_ITERS),
n_samples(NN_N_SAMPLES),
upd_thresh(NN_UPD_THRESH),
corner_sigma_d(NN_CORNER_SIGMA_D),
corner_sigma_t(NN_CORNER_SIGMA_T),
pix_sigma(NN_PIX_SIGMA),
index_type(static_cast<NNIndex::IdxType>(NN_INDEX_TYPE)),
n_checks(NN_N_CHECKS),
additive_update(NN_ADDITIVE_UPDATE),
direct_samples(NN_DIRECT_SAMPLES),
ssm_sigma_prec(NN_SSM_SIGMA_PREC),
debug_mode(NN_DEBUG_MODE){
	if(params){
		max_iters = params->max_iters;
		n_samples = params->n_samples;
		upd_thresh = params->upd_thresh;

		ssm_sigma = params->ssm_sigma;
		corner_sigma_d = params->corner_sigma_d;
		corner_sigma_t = params->corner_sigma_t;
		pix_sigma = params->pix_sigma;

		index_type = params->index_type;
		n_checks = params->n_checks;
		additive_update = params->additive_update;
		direct_samples = params->direct_samples;
		ssm_sigma_prec = params->ssm_sigma_prec;
		debug_mode = params->debug_mode;
	}
}

_MTF_END_NAMESPACE

#include "mtfRegister.h"
_REGISTER_TRACKERS(NN);

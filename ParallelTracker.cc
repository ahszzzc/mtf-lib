#include "ParallelTracker.h"

_MTF_BEGIN_NAMESPACE

const char* ParallelParams::operator()(EstimationMethod _estimation_method) const {
	switch(_estimation_method) {
	case EstimationMethod::MeanOfCorners:
		return "MeanOfCorners";
	case EstimationMethod::MeanOfState:
		return "MeanOfState";
	default:
		throw std::invalid_argument("Invalid dynamic model provided");
	}
}
ParallelParams::ParallelParams(EstimationMethod _estimation_method, bool _reset_to_mean) {
	estimation_method = _estimation_method;
	reset_to_mean = _reset_to_mean;
}
ParallelParams::ParallelParams(ParallelParams *params) :
estimation_method(static_cast<EstimationMethod>(PARL_ESTIMATION_METHOD)),
reset_to_mean(PARL_RESET_TO_MEAN) {
	if(params) {
		estimation_method = params->estimation_method;
		reset_to_mean = params->reset_to_mean;
	}
}

template<class AM, class SSM>
ParallelTracker<AM, SSM>::ParallelTracker(const vector<SM*> _trackers, ParamType *parl_params,
	int resx, int resy, SSMParams *ssm_params) :
	SM(), curr_img_updated(false),
	params(parl_params), trackers(_trackers) {
	n_trackers = trackers.size();

	printf("\n");
	printf("Initializing Parallel tracker with:\n");
	printf("estimation_method: %d :: %s\n", params.estimation_method,
		params(params.estimation_method));
	printf("reset_to_mean: %d\n", params.reset_to_mean);
	printf("n_trackers: %d\n", n_trackers);
	printf("trackers: ");
	for(int i = 0; i < n_trackers; i++) {
		name = name + trackers[i]->name + " ";
		printf("%d: %s ", i + 1, trackers[i]->name.c_str());
	}
	printf("\n");
	ssm = new SSM(resx, resx, ssm_params);

	switch(params.estimation_method) {
	case EstimationMethod::MeanOfCorners:
		mean_corners_cv.create(2, 4, CV_64FC1);
		break;
	case EstimationMethod::MeanOfState:
		ssm_state_size = ssm->getStateSize();
		ssm_states.resize(n_trackers);
		for(int tracker_id = 0; tracker_id < n_trackers; tracker_id++) {
			ssm_states[tracker_id].resize(ssm_state_size);
		}
		mean_state.resize(ssm_state_size);
		break;
	}
}
template<class AM, class SSM>
void ParallelTracker<AM, SSM>::initialize(const cv::Mat &img, const cv::Mat &corners)  {
	curr_img = img;
	curr_img_updated = true;
}
template<class AM, class SSM>
void ParallelTracker<AM, SSM>::update(const cv::Mat &img)  {
	curr_img = img;
	curr_img_updated = true;
}

template<class AM, class SSM>
void ParallelTracker<AM, SSM>::initialize(const cv::Mat &corners)  {
	for(int tracker_id = 0; tracker_id < n_trackers; tracker_id++) {
		if(curr_img_updated) {
			trackers[tracker_id]->initialize(curr_img, corners);
		} else {
			trackers[tracker_id]->initialize(corners);
		}
	}
	ssm->initialize(corners);
	ssm->getCorners(cv_corners);
}
template<class AM, class SSM>
void ParallelTracker<AM, SSM>::update()  {
	mean_corners.setZero();
	mean_corners_cv.setTo(cv::Scalar(0));
#ifdef ENABLE_TBB
	parallel_for(tbb::blocked_range<size_t>(0, n_trackers),
		[&](const tbb::blocked_range<size_t>& r) {
		for(size_t tracker_id = r.begin(); tracker_id != r.end(); ++tracker_id) {
#else
	for(int tracker_id = 0; tracker_id < n_trackers; ++tracker_id) {
#endif
		if(curr_img_updated) {
			trackers[tracker_id]->update(curr_img);
		} else {
			trackers[tracker_id]->update();
		}

		switch(params.estimation_method) {
		case EstimationMethod::MeanOfCorners:
			mean_corners += (trackers[tracker_id]->ssm->getCorners() - mean_corners) / (tracker_id + 1);
			mean_corners_cv += (trackers[tracker_id]->getRegion() - mean_corners_cv) / (tracker_id + 1);
			break;
		case EstimationMethod::MeanOfState:
			ssm_states[tracker_id] = trackers[tracker_id]->getSSM()->getState();
			break;
		}
	}
#ifdef ENABLE_TBB
		});
#endif
		switch(params.estimation_method) {
		case EstimationMethod::MeanOfCorners:
			ssm->setCorners(mean_corners);
			break;
		case EstimationMethod::MeanOfState:
			ssm->estimateMeanOfSamples(mean_state, ssm_states, n_trackers);
			ssm->setState(mean_state);
			ssm->getCorners(mean_corners_cv);
			break;
		}

		if(params.reset_to_mean) {
			for(int tracker_id = 0; tracker_id < n_trackers; tracker_id++) {
				trackers[tracker_id]->setRegion(mean_corners_cv);
			}
		}
		ssm->getCorners(cv_corners);
}
template<class AM, class SSM>
const cv::Mat& ParallelTracker<AM, SSM>::getRegion()  {
	ssm->getCorners(cv_corners_mat);
	return cv_corners_mat;
}

template<class AM, class SSM>
void ParallelTracker<AM, SSM>::setRegion(const cv::Mat& corners)   {
	for(int tracker_id = 1; tracker_id < n_trackers; tracker_id++) {
		trackers[tracker_id]->setRegion(corners);
	}
	ssm->setCorners(corners);
}


ParallelTracker2::ParallelTracker2(const vector<TrackerBase*> _trackers, ParamType *parl_params) :
TrackerBase(), curr_img_updated(false),
params(parl_params), trackers(_trackers) {
	n_trackers = trackers.size();

	printf("\n");
	printf("Initializing generalized Parallel tracker with:\n");
	printf("reset_to_mean: %d\n", params.reset_to_mean);
	printf("n_trackers: %d\n", n_trackers);
	printf("trackers: ");
	for(int i = 0; i < n_trackers; i++) {
		name = name + trackers[i]->name + " ";
		printf("%d: %s ", i + 1, trackers[i]->name.c_str());
	}
	printf("\n");

	mean_corners_cv.create(2, 4, CV_64FC1);
}

void ParallelTracker2::initialize(const cv::Mat &img, const cv::Mat &corners)  {
	curr_img = img;
	curr_img_updated = true;
}

void ParallelTracker2::update(const cv::Mat &img)  {
	curr_img = img;
	curr_img_updated = true;
}


void ParallelTracker2::initialize(const cv::Mat &corners)  {
	for(int tracker_id = 0; tracker_id < n_trackers; tracker_id++) {
		if(curr_img_updated) {
			trackers[tracker_id]->initialize(curr_img, corners);
		} else {
			trackers[tracker_id]->initialize(corners);
		}
	}
	convertMatToPoint2D(cv_corners, corners);
}

void ParallelTracker2::update()  {
	mean_corners_cv.setTo(cv::Scalar(0));
#ifdef ENABLE_TBB
	parallel_for(tbb::blocked_range<size_t>(0, n_trackers),
		[&](const tbb::blocked_range<size_t>& r) {
		for(size_t tracker_id = r.begin(); tracker_id != r.end(); ++tracker_id) {
#else
	for(int tracker_id = 0; tracker_id < n_trackers; ++tracker_id) {
#endif
		if(curr_img_updated) {
			trackers[tracker_id]->update(curr_img);
		} else {
			trackers[tracker_id]->update();
		}
		mean_corners_cv += (trackers[tracker_id]->getRegion() - mean_corners_cv) / (tracker_id + 1);
	}
#ifdef ENABLE_TBB
		});
#endif
	if(params.reset_to_mean) {
		for(int tracker_id = 0; tracker_id < n_trackers; tracker_id++) {
			trackers[tracker_id]->setRegion(mean_corners_cv);
		}
	}
	convertMatToPoint2D(cv_corners, mean_corners_cv);
}


void ParallelTracker2::setRegion(const cv::Mat& corners)   {
	for(int tracker_id = 1; tracker_id < n_trackers; tracker_id++) {
		trackers[tracker_id]->setRegion(corners);
	}
	corners.copyTo(mean_corners_cv);
}

void ParallelTracker2::convertMatToPoint2D(cv::Point2d *cv_corners,
	const cv::Mat &cv_corners_mat) {
	for(int i = 0; i < 4; i++) {
		cv_corners[i].x = cv_corners_mat.at<double>(0, i);
		cv_corners[i].y = cv_corners_mat.at<double>(1, i);
	}
}
_MTF_END_NAMESPACE

#include "mtfRegister.h"
_REGISTER_TRACKERS(ParallelTracker);
#include "GNN.h"
#include <time.h>
#include <ctime> 

_MTF_BEGIN_NAMESPACE


template <class AM, class SSM>
GNN<AM, SSM >::GNN(ParamType *nn_params,
	AMParams *am_params, SSMParams *ssm_params) :
	SearchMethod<AM, SSM>(am_params, ssm_params),
	params(nn_params){

	printf("\n");
	printf("initializing Graph based Nearest Neighbor tracker with:\n");
	printf("max_iters: %d\n", params.max_iters);
	printf("n_samples: %d\n", params.n_samples);
	printf("k: %d\n", params.k);
	printf("upd_thresh: %f\n", params.upd_thresh);
	printf("corner_sigma_d: %f\n", params.corner_sigma_d);
	printf("corner_sigma_t: %f\n", params.corner_sigma_t);
	printf("additive_update: %d\n", params.additive_update);
	printf("direct_samples: %d\n", params.direct_samples);
	printf("debug_mode: %d\n", params.debug_mode);


	printf("appearance model: %s\n", am->name.c_str());
	printf("state space model: %s\n", ssm->name.c_str());
	printf("\n");

	name = "nn";
	log_fname = "log/mtf_nn_log.txt";
	time_fname = "log/mtf_nn_times.txt";
	frame_id = 0;

	ssm_state_size = ssm->getStateSize();
	am_feat_size = am->getDistFeatSize();

	printf("ssm_state_size: %d\n", ssm_state_size);
	printf("am_feat_size: %d\n", am_feat_size);

	//flann_query = new flannMatType(const_cast<double*>(eig_query.data()), 
	//	eig_query.rows(), eig_query.cols());

	//flann_result = new flannResultType(const_cast<int*>(eig_result.data()),
	//	eig_result.rows(), eig_result.cols());
	//flann_dists = new flannMatType(const_cast<double *>(eig_dists.data()),
	//	eig_dists.rows(), eig_dists.cols());

	ssm_update.resize(ssm_state_size);
	ssm_sigma.resize(ssm_state_size);
	ssm_perturbations.resize(ssm_state_size, params.n_samples);
	inv_state_update.resize(ssm_state_size);
}

template <class AM, class SSM>
void GNN<AM, SSM >::initialize(const cv::Mat &corners){

	start_timer();

	eig_dataset.resize(params.n_samples, am_feat_size);
	//eig_query.resize(am_feat_size);
	eig_result.resize(1);
	eig_dists.resize(1);

	ssm->initialize(corners);

	am->initializePixVals(ssm->getPts());
	am->initializeDistFeat();

	baseGeneratorType generator(rand());
	dustributionType gaussian_dist_d(0, params.corner_sigma_d);
	dustributionType gaussian_dist_t(0, params.corner_sigma_t);
	randomGeneratorType rand_gen_d(generator, gaussian_dist_d);
	randomGeneratorType rand_gen_t(generator, gaussian_dist_t);

	Matrix24d rand_d;
	Vector2d rand_t;
	Matrix24d disturbed_corners;
	VectorXd state_update(ssm_state_size);

	printf("building feature dataset...\n");
	for(int sample_id = 0; sample_id < params.n_samples; sample_id++){

		rand_t(0) = rand_gen_t();
		rand_t(1) = rand_gen_t();
		for(int i = 0; i < 4; i++){
			rand_d(0, i) = rand_gen_d();
			rand_d(1, i) = rand_gen_d();
		}
		disturbed_corners = ssm->getCorners() + rand_d;
		disturbed_corners = disturbed_corners.colwise() + rand_t;
		ssm->estimateWarpFromCorners(state_update, ssm->getCorners(), disturbed_corners);

		if(params.additive_update){
			inv_state_update = -state_update;
			ssm->additiveUpdate(inv_state_update);
		} else{
			ssm->invertState(inv_state_update, state_update);
			ssm->compositionalUpdate(inv_state_update);
		}

		am->updatePixVals(ssm->getPts());
		am->updateDistFeat(eig_dataset.row(sample_id).data());


		ssm_perturbations.col(sample_id) = state_update;

		// reset SSM to previous state
		if(params.additive_update){
			ssm->additiveUpdate(state_update);
		} else{
			ssm->compositionalUpdate(state_update);
		}
	}

	// Build graph
	printf("Building the graph...\n");
	Nodes = gnn::build_graph<AM>(eig_dataset.data(), params.k, params.n_samples, am_feat_size, *am);

	ssm->getCorners(cv_corners);

	end_timer();
	write_interval(time_fname, "w");
}

template <class AM, class SSM>
void GNN<AM, SSM >::update(){
	++frame_id;
	write_frame_id(frame_id);

	am->setFirstIter();
	for(int i = 0; i < params.max_iters; i++){
		init_timer();

		am->updatePixVals(ssm->getPts());
		record_event("am->updatePixVals");

		am->updateDistFeat();
		record_event("am->updateDistFeat");

		struct gnn::indx_dist * knns = gnn::search_graph<AM>(Nodes, const_cast<double*>(am->getDistFeat()),
			eig_dataset.data(), params.k, 1, params.n_samples, am_feat_size, *am);

		printf("Best match found at index %d with distance %f\n", knns[0].idx, knns[0].dist);

		ssm_update = ssm_perturbations.col(knns[0].idx);

		prev_corners = ssm->getCorners();

		if(params.additive_update){
			ssm->additiveUpdate(ssm_update);
			record_event("ssm->additiveUpdate");
		} else{
			ssm->compositionalUpdate(ssm_update);
			record_event("ssm->compositionalUpdate");
		}

		double update_norm = (prev_corners - ssm->getCorners()).squaredNorm();
		record_event("update_norm");

		write_data(time_fname);

		if(update_norm < params.upd_thresh){
			if(params.debug_mode){
				printf("n_iters: %d\n", i + 1);
			}
			break;
		}

		am->clearFirstIter();
	}
	ssm->getCorners(cv_corners);
}

_MTF_END_NAMESPACE

#include "mtfRegister.h"
_REGISTER_TRACKERS(GNN);

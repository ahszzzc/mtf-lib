#ifndef RKLT_H
#define RKLT_H

#include "SearchMethod.h"
#include "GridTracker.h"

#define RKLT_ENABLE_SPI true
#define RKLT_ENABLE_FEEDBACK true
#define RKLT_FAILURE_DETECTION true
#define RKLT_FAILURE_THRESH 15.0
#define RKLT_DEBUG_MODE false

_MTF_BEGIN_NAMESPACE

struct RKLTParams{
	bool enable_spi; 
	bool enable_feedback;
	bool failure_detection;
	double failure_thresh;
	bool debug_mode;

	RKLTParams(bool _enable_spi, bool _enable_feedback,
		bool _failure_detection, double _failure_thresh,
		bool _debug_mode){
		enable_spi = _enable_spi;
		enable_feedback = _enable_feedback;
		failure_detection = _failure_detection;
		failure_thresh = _failure_thresh;
		debug_mode = _debug_mode;
	}
	RKLTParams(RKLTParams *params = nullptr) :
		enable_spi(RKLT_ENABLE_SPI),
		enable_feedback(RKLT_ENABLE_FEEDBACK),
		failure_detection(RKLT_FAILURE_DETECTION),
		failure_thresh(RKLT_FAILURE_THRESH),
		debug_mode(RKLT_DEBUG_MODE){
		if(params){
			enable_spi = params->enable_spi;
			enable_feedback = params->enable_feedback;
			failure_detection = params->failure_detection;
			failure_thresh = params->failure_thresh;
			debug_mode = params->debug_mode;
		}
	}
};

template<class AM, class SSM, class GridSSM>
class RKLT : public TrackerBase {

public:

	typedef SearchMethod < AM, SSM > TemplTrackerType;
	typedef GridTracker < GridSSM > GridTrackerType;

	typedef RKLTParams ParamType;
	ParamType params;

	TemplTrackerType *templ_tracker;
	GridTrackerType * grid_tracker;

	cv::Mat grid_corners_mat;

	RKLT(ParamType *rklt_params, 
		GridTrackerType *_grid_tracker, TemplTrackerType *_templ_tracker) :
		TrackerBase(), params(rklt_params),
		templ_tracker(_templ_tracker), grid_tracker(_grid_tracker){
		printf("\n");
		printf("Initializing RKL tracker with:\n");
		printf("enable_spi: %d\n", params.enable_spi);
		printf("enable_feedback: %d\n", params.enable_feedback);
		printf("failure_detection: %d\n", params.failure_detection);
		printf("failure_thresh: %f\n", params.failure_thresh);

		printf("debug_mode: %d\n", params.debug_mode);
		printf("GridTracker state space model: %s\n", grid_tracker->ssm->name.c_str());
		printf("templ_tracker: %s with:\n", templ_tracker->name.c_str());
		printf("\t appearance model: %s\n", templ_tracker->am->name.c_str());
		printf("\t state space model: %s\n", templ_tracker->ssm->name.c_str());
		printf("\n");

		if(params.enable_spi){
			if(!templ_tracker->supportsSPI()){
				printf("Template tracker does not support SPI so disabling it\n");
				params.enable_spi = false;
			} else if(grid_tracker->getResX() != templ_tracker->am->getResX() || 
				grid_tracker->getResY() != templ_tracker->am->getResY()){
				printf("Sampling resolution of the template tracker: %d x %d is not same as the grid size: %d x %d so disabling it\n",
					templ_tracker->am->getResX(), templ_tracker->am->getResY(), grid_tracker->getResX(), grid_tracker->getResY());
				params.enable_spi = false;
			} else{
				printf("SPI is enabled\n");
			}
		}
		if(params.failure_detection){
			printf("Template tracker failure detection is enabled with a threshold of %f\n", 
				params.failure_thresh);
			grid_corners_mat.create(2, 4, CV_64FC1);
		}
	}

	void initialize(const cv::Mat &cv_img, const cv::Mat &corners) override {
		grid_tracker->initialize(cv_img, corners);
		templ_tracker->initialize(cv_img, corners);

		cv_corners_mat = templ_tracker->getRegion();
		convertMatToPoint2D(cv_corners, cv_corners_mat);
	}
	void initialize(const cv::Mat &corners) override {
		grid_tracker->initialize(corners);
		templ_tracker->initialize(corners);

		cv_corners_mat = templ_tracker->getRegion();
		convertMatToPoint2D(cv_corners, cv_corners_mat);
	}

	void update(const cv::Mat &cv_img) override {
		grid_tracker->update(cv_img);
		templ_tracker->setRegion(grid_tracker->getRegion());
		if(params.enable_spi){
			templ_tracker->setSPIMask((const bool*)grid_tracker->getPixMask());
		}
		templ_tracker->update(cv_img);
		postUpdateProc();
	}

	void update() override {
		grid_tracker->update();
		templ_tracker->setRegion(grid_tracker->getRegion());
		if(params.enable_spi){
			templ_tracker->setSPIMask((const bool*)grid_tracker->getPixMask());
		}
		templ_tracker->update();		
		postUpdateProc();
	}
	void postUpdateProc(){
		cv_corners_mat = templ_tracker->getRegion();
		if(params.failure_detection){
			grid_corners_mat = grid_tracker->getRegion();
			double corner_diff = cv::norm(cv_corners_mat, grid_corners_mat);
			if(corner_diff>params.failure_thresh){
				convertMatToPoint2D(cv_corners, grid_corners_mat);
				return;
			}
		}
		if(params.enable_feedback){
			grid_tracker->setRegion(cv_corners_mat);
		}
		convertMatToPoint2D(cv_corners, cv_corners_mat);
	}
private:
	~RKLT(){}
	inline void convertMatToPoint2D(cv::Point2d *corners,
		const cv::Mat &corners_mat){
		for(int i = 0; i < 4; i++){
			corners[i].x = corners_mat.at<double>(0, i);
			corners[i].y = corners_mat.at<double>(1, i);
		}
	}
};
_MTF_END_NAMESPACE

#endif


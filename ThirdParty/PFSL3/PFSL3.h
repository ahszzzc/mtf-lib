#include <ctime>
#include <opencv/cv.h>
#include <opencv/highgui.h>
#include <opencv/cxcore.h>

#include "../../TrackerBase.h"

//#include <inttypes.h>
//typedef int64_t __int64;

#define MMAX(x,y) ( (x) > (y) ? (x) : (y) )
#define MMIN(x,y) ( (x) < (y) ? (x) : (y) )

#define PFSL3_P_X 40
#define PFSL3_P_Y 40
#define PFSL3_ROT 0
#define PFSL3_NCC_STD 0.1
#define PFSL3_PCA_STD 10
#define PFSL3_STATE_SIG 
#define PFSL3_AR_P 0.5
#define PFSL3_N 40
#define PFSL3_N_C 10
#define PFSL3_N_ITER 5
#define PFSL3_SAMPLING 1
#define PFSL3_CAPTURE 0
#define PFSL3_MEAN_CHECK 0
#define PFSL3_OUTLIER_FLAG 0
#define PFSL3_LEN 100
#define PFSL3_INIT_SIZE 15
#define PFSL3_UPDATE_PERIOD 5
#define PFSL3_FF 0.99
#define PFSL3_BASIS_THR 0.95
#define PFSL3_MAX_NUM_BASIS 30
#define PFSL3_MAX_NUM_USED_BASIS 10

struct PFSL3Params {
    int p_x, p_y;
    std::vector<double>state_sig;
    double rot, NCC_std, PCA_std, AR_p;
    int N, N_c, N_iter, sampling, capture, mean_check, outlier_flag;
    int len;

    int init_size, update_period;
    float ff;
    double basis_thr;
    int max_num_basis, max_num_used_basis;


    PFSL3Params(int _p_x, int _p_y,
                const std::vector<double> &_state_sig,
                double _rot, double _NCC_std, double _PCA_std, double _AR_p,
                int _N, int _N_c, int _N_iter, int _sampling, int _capture,
                int _mean_check, int _outlier_flag, int _len,
                int _init_size, int _update_period, float _ff,
                double _basis_thr, int _max_num_basis, int _max_num_used_basis) {

        p_x = _p_x;
        p_y = _p_y;
        state_sig = _state_sig;
        rot = _rot;
        NCC_std = _NCC_std;
        PCA_std = _PCA_std;
        AR_p = _AR_p;
        N = _N;
        N_iter = _N_iter;
        sampling = _sampling;
        capture = _capture;
        mean_check = _mean_check;
        outlier_flag = _outlier_flag;
        len = _len;

        init_size = _init_size;
        update_period = _update_period;
        ff = _ff;
        basis_thr = _basis_thr;
        max_num_basis = _max_num_basis;
        max_num_used_basis = _max_num_used_basis;
    }
    PFSL3Params(PFSL3Params *params = NULL) :
        p_x(PFSL3_P_X),
        p_y(PFSL3_P_Y),
        rot(PFSL3_ROT),
        NCC_std(PFSL3_NCC_STD),
        PCA_std(PFSL3_PCA_STD),
        AR_p(PFSL3_AR_P),
        N(PFSL3_N),
        N_c(PFSL3_N_C),
        N_iter(PFSL3_N_ITER),
        sampling(PFSL3_SAMPLING),
        capture(PFSL3_CAPTURE),
        mean_check(PFSL3_MEAN_CHECK),
        outlier_flag(PFSL3_OUTLIER_FLAG),
        len(PFSL3_LEN),
        init_size(PFSL3_INIT_SIZE),
		update_period(PFSL3_UPDATE_PERIOD),
		ff(PFSL3_FF),
		basis_thr(PFSL3_BASIS_THR),
		max_num_basis(PFSL3_MAX_NUM_BASIS),
		max_num_used_basis(PFSL3_MAX_NUM_USED_BASIS) {

		state_sig.push_back(3.5);
		state_sig.push_back(3.5);
		state_sig.push_back(0.04);
		state_sig.push_back(0.015);
		state_sig.push_back(0.015);
		state_sig.push_back(0.015);
		state_sig.push_back(0.0003);
		state_sig.push_back(0.0003);

        if(params) {
            p_x = params->p_x;
            p_y = params->p_y;
            state_sig = params->state_sig;
            rot = params->rot;
            NCC_std = params->NCC_std;
            PCA_std = params->PCA_std;
            AR_p = params->AR_p;
            N = params->N;
            N_iter = params->N_iter;
            sampling = params->sampling;
            capture = params->capture;
            mean_check = params->mean_check;
            outlier_flag = params->outlier_flag;
            len = params->len;

            init_size = params->init_size;
            update_period = params->update_period;
            ff = params->ff;
            basis_thr = params->basis_thr;
            max_num_basis = params->max_num_basis;
            max_num_used_basis = params->max_num_used_basis;
        }
    }
};

class PFSL3: public mtf::TrackerBase {

    int p_x, p_y;
    double c_x, c_y, l_x, l_y;
    int t;

    CvSize frame_size;
    CvMat* c_p;
    CvMat* rc_p;
    CvMat* temp_cp;
    CvMat* temp_rcp;
    CvMat* inv_temp_rcp;
    CvMat* transform;
    double p_transform[6];
    double* p_matrix;
    double init_h[9];
    CvMat* obj_template;
    CvMat warped_img_2D_header, *warped_img_2D;
    float* tracked_img;

    CvMat* t_point;
    CvPoint p1;
    CvPoint p2;
    CvPoint p3;
    CvPoint p4;
    CvMat* dX_du;
    CvMat* grad_x, *grad_y, *jacobian, *adj_template;
    CvScalar mean_v;

    double* X_par;
    double* X_pred;
    double* AR_velocity;
    double* AR_pred;
    double* mean_X_par;
    double* prob;
    double* w;

    double u, v, NCC, dist, prob_sum;
    double msec;

    double random_coeff[8];
    double scaled_rand[8];
    double sl3_rand[9];
    double previous_X[9];
    double pred_X[9];
    double AR[9];
    double log_AR[9];
    double a_log_AR[9];
    double _update[9];
    double update_exp[9];
    double pred_AR[9];
    double inv_X[9];

    int previous_index;
    double* Mean;

    double* Cov;
    double state_cov[64];

    double diff1;
    double X_diff[8];
    double diff2;
    double* pred_measure;
    double NCC_real, PCA_real;

    CvMat* Init_Cov, *NCC_jacobian, *NCC_J_temp1, *NCC_J_temp2;
    double mu_2;
    CvMat* sigma_11, *sigma_12, *sigma_22, *temp_like_NCC;

    int best_index;
    double New_Cov[64];
    double New_Mean[9];
    double Prev_Cov[64];
    double Prev_Mean[9];

    int num_tracked_img;

    int num_basis;
    int num_used_basis;

    CvMat* basis, *projected, *data_proj, *diff_img, *temp_basis;

    float proj_sum;
    float* mean_img;
    float* mean_update_img;
    float* mean_adj_img;
    float* update_img;
    float* new_data;

    int num_update_img;
    int PCA_flag;
    double PCA;

    double max_X[9];
    CvMat* cv_mean_img, *data_res, *templ, *divide;
    CvRNG rng_state;
    CvMat* random, *previous_X3x3, *warped_img, *adj_warped_img, *divide_prob;
    CvMat* temp_MAT, *temp_w;

    double increment[8];
    double update_NCC_thr;
    CvMat* inv_sigma_22, *temp_Cov1, *temp_Cov2, *temp_increment, *temp_pred_measure;
    CvPoint min_loc;
    CvPoint max_loc;

    char UPLO;


    double X_par_temp[9];
    double dist1, dist2, dist3;
    double min_v, max_v;
    double t_mu_2[2];

    CvMat* CH, *colored_random, *dist3_temp1, *dist3_temp2, *COV3, *inv_COV3;
    CvPoint min_ind;
    CvPoint max_ind;
    CvMat* X_diff_cv, *PCA_recon, *PCA_recon_weight, *PCA_recon_temp, *mean_jacobian, *d_PCA_measure;
    CvMat* PCA_jacobian, *total_jacobian, *t_sigma_12, *t_sigma_22, *total_like, *inv_t_sigma_22, *t_temp_Cov1;
    CvMat* weight_plot, *recon_error, *dist2_temp1, *dist2_temp2, *COV2, *inv_COV2;

    double update_PCA_thr;
    double num_est;
    double outlier_thr;

    float weight;
    int num_outlier;
    double update_thr;

    CvMat* QR_Q, *Q_proj, *W, *U, *V, *A, *Sing_val, *bestindex_plot;
    clock_t begin, end;
    int* outindex;

    float* temp_img;

    int* outindex2;
    double* X_par_mean;
    double* AR_velocity_mean;
    double* new_NCC_sig;

    double NCC_J_norm;
    double PCA_J_norm;

    void imwarping_NN(CvMat* img, double* h_matrix, double* p_matrix, double* p_transform, int c_x, int c_y, int p_x, int p_y, CvSize frame_size, CvMat* warped_img);
    void imwarping_BL(CvMat* img, double* h_matrix, double* p_matrix, double* p_transform, int c_x, int c_y, int p_x, int p_y, CvSize frame_size, CvMat* warped_img);
    void point_transform(CvMat* point, double* h_matrix, double* p_transform, int c_x, int c_y, CvMat* t_point);
    void mat_exp(double* input, double* output);
    void mat_log(double* input, double* output);
    void mat_mul_3x3(double* input_A, double* input_B, double* output);
    void sl3_to_vec(double* input, double* output);
    void sl3(double* input, double* output);
    void mat_scalar(double* input, double scalar, double* output);
    void mat_add(double* input_A, double* input_B, double* output);
    void mat_mul_elem(double* input_A, double* input_B, double* output);
    void mat_display(double* input, int size);
    void resampling(double* w, int N, int M, int* outindex);
    void particle_copy(int* outindex, int N, double* input_A, double* input_B, double* output_A, double* output_B);
    double elem_sum(double* input, int N);
    void elem_div(double* input, double div, double *output, int N);
    void sample_mean(double* X_par, double* X_max, int N, double* Mean_X);
    void mat_inv_3x3(double* input, double* output);
    void cal_mean_img(float* input, int length, int num_img, float* output);
    void cal_mean_adj_img(float* input, float* mean_img, int length, int num_img, float* output);
    int find_index(CvMat* W, double threshold);
    void mean_update(float* mean_img, float* mean_update_img, float ff, int update_period, int num_tracked_img, int length);
    void img_diff_scalar_mul(float* input_A, float* input_B, float scalar, int length, float* output);
    void jacobian_calculation(double* p_matrix, CvMat* grad_x, CvMat* grad_y, CvMat* dX_du, int p_x, int p_y, CvMat* jacobian);
    void image_gradient(CvMat* input, int p_x, int p_y, CvMat* grad_x, CvMat* grad_y);

public:

    typedef PFSL3Params ParamType;
    ParamType params;

    CvMat curr_img;
    CvMat *img;

    PFSL3(const cv::Mat &_img,
          ParamType *pfsl3_params = NULL):
        TrackerBase(),
        params(pfsl3_params) {
        name = "pfsl3";
        curr_img = _img;
        img = &curr_img;

        printf("\n");
        printf("Initializing PF SL3 tracker\n");
        printf("\n");
    }
    virtual ~PFSL3() {}

	bool rgbInput() const { return false; }

    void initialize(const cv::Mat &_img, const cv::Mat &corners) {
        curr_img = _img;
        img = &curr_img;
        initialize(corners);
    }
    void initialize(const cv::Mat&cv_corners);

    void update(const cv::Mat &_img) {
        curr_img = _img;
        img = &curr_img;
        update();
    }
    void update();
    void updateCVCorners() {
        point_transform(rc_p, &mean_X_par[0], &p_transform[0], c_x, c_y, t_point);
        cv_corners[0] = cv::Point(CV_MAT_ELEM(*t_point, double, 0, 1) + 0.5f - 1, CV_MAT_ELEM(*t_point, double, 0, 0) + 0.5f - 1);
        cv_corners[1] = cv::Point((CV_MAT_ELEM(*t_point, double, 0, 3) + 0.5f) - 1, (CV_MAT_ELEM(*t_point, double, 0, 2) + 0.5f) + 1);
        cv_corners[2] = cv::Point((CV_MAT_ELEM(*t_point, double, 0, 5) + 0.5f) + 1, (CV_MAT_ELEM(*t_point, double, 0, 4) + 0.5f) + 1);
        cv_corners[3] = cv::Point((CV_MAT_ELEM(*t_point, double, 0, 7) + 0.5f) + 1, (CV_MAT_ELEM(*t_point, double, 0, 6) + 0.5f) - 1);
    }

	void convertMatToPoint2D(cv::Point2d *corners,
		const cv::Mat &corners_mat){
		for(int i = 0; i < 4; i++){
			corners[i].x = corners_mat.at<double>(0, i);
			corners[i].y = corners_mat.at<double>(1, i);
		}
	}
};


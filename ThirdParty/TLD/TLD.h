/*  Copyright 2011 AIT Austrian Institute of Technology
*
*   This file is part of OpenTLD.
*
*   OpenTLD is free software: you can redistribute it and/or modify
*   it under the terms of the GNU General Public License as published by
*    the Free Software Foundation, either version 3 of the License, or
*   (at your option) any later version.
*
*   OpenTLD is distributed in the hope that it will be useful,
*   but WITHOUT ANY WARRANTY; without even the implied warranty of
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*   GNU General Public License for more details.
*
*   You should have received a copy of the GNU General Public License
*   along with OpenTLD.  If not, see <http://www.gnu.org/licenses/>.
*
*/

/*
 * TLD.h
 *
 *  Created on: Nov 17, 2011
 *      Author: Georg Nebehay
 */

#ifndef TLD_H_
#define TLD_H_

#include <opencv/cv.h>

#include "MedianFlowTracker.h"
#include "DetectorCascade.h"
#include "../../TrackerBase.h"

struct TLDParams{
	bool trackerEnabled;
	bool detectorEnabled;
	bool learningEnabled;
	bool alternating;

	TLDParams(bool trackerEnabled, bool detectorEnabled,
		bool learningEnabled, bool alternating){
		this->trackerEnabled = trackerEnabled;
		this->detectorEnabled = detectorEnabled;
		this->learningEnabled = learningEnabled;
		this->alternating = alternating;
	}

	TLDParams(TLDParams *params = NULL) : 
		trackerEnabled(true), 
		detectorEnabled(true),
		learningEnabled(true), 
		alternating(false){
		if(params){
			trackerEnabled = params->trackerEnabled;
			detectorEnabled = params->detectorEnabled;
			learningEnabled = params->learningEnabled;
			alternating = params->alternating;
		}
	}
};

namespace tld
{

	class TLD : public mtf::TrackerBase
	{
		void storeCurrentData();
		void fuseHypotheses();
		void learn();
		void initialLearning();
	public:
		typedef TLDParams ParamType;
		ParamType params;

		bool trackerEnabled;
		bool detectorEnabled;
		bool learningEnabled;
		bool alternating;

		MedianFlowTracker *medianFlowTracker;
		DetectorCascade *detectorCascade;
		NNClassifier *nnClassifier;
		bool valid;
		bool wasValid;
		cv::Mat prevImg;
		cv::Mat currImg;
		cv::Rect *prevBB;
		cv::Rect *currBB;
		float currConf;
		bool learning;

		cv::Mat cv_img;

		TLD();
		TLD(const cv::Mat &img, ParamType *tld_params = NULL) : 
			TrackerBase(), params(tld_params){
			name = "tld";

			trackerEnabled = params.trackerEnabled;
			detectorEnabled = params.detectorEnabled;
			learningEnabled = params.learningEnabled;
			alternating = params.alternating;

			printf("\n");
			printf("Initializing TLD tracker with:\n");
			printf("trackerEnabled: %d\n", params.trackerEnabled);
			printf("detectorEnabled: %d\n", params.detectorEnabled);
			printf("learningEnabled: %d\n", params.learningEnabled);
			printf("alternating: %d\n", params.alternating);
			printf("\n");

			valid = false;
			wasValid = false;
			learning = false;
			currBB = NULL;
			prevBB = new cv::Rect(0, 0, 0, 0);

			detectorCascade = new DetectorCascade();
			nnClassifier = detectorCascade->nnClassifier;

			medianFlowTracker = new MedianFlowTracker();
		}
		virtual ~TLD();
		bool rgbInput() const  { return true; }

		void release();
		void selectObject(const cv::Mat &img, cv::Rect *bb);
		void processImage(const cv::Mat &img);
		void writeToFile(const char *path);
		void readFromFile(const char *path);

		void initialize(const cv::Mat &img, const cv::Mat &corners){

			cv::cvtColor(img, cv_img, CV_BGR2GRAY);
			printf("initialize::img_height: %d\n", cv_img.rows);
			printf("initialize::img_width: %d\n", cv_img.cols);
			initialize(corners);
		}
		void initialize(const cv::Mat& cv_corners){
			//double pos_x = (cv_corners.at<double>(0, 0) + cv_corners.at<double>(0, 1) +
			//	cv_corners.at<double>(0, 2) + cv_corners.at<double>(0, 3)) / 4;
			//double pos_y = (cv_corners.at<double>(1, 0) + cv_corners.at<double>(1, 1) +
			//	cv_corners.at<double>(1, 2) + cv_corners.at<double>(1, 3)) / 4;

			double pos_x = cv_corners.at<double>(0, 0);
			double pos_y = cv_corners.at<double>(1, 0);
			double size_x = ((cv_corners.at<double>(0, 1) - cv_corners.at<double>(0, 0)) +
				(cv_corners.at<double>(0, 2) - cv_corners.at<double>(0, 3))) / 2;
			double size_y = ((cv_corners.at<double>(1, 3) - cv_corners.at<double>(1, 0)) +
				(cv_corners.at<double>(1, 2) - cv_corners.at<double>(1, 1))) / 2;

			printf("initialize::pos_x: %f\n", pos_x);
			printf("initialize::pos_y: %f\n", pos_y);
			printf("initialize::size_x: %f\n", size_x);
			printf("initialize::size_y: %f\n", size_y);

			cv::Rect init_rect(pos_x, pos_y, size_x, size_y);

			printf("initialize::init_rect.x: %d\n", init_rect.x);
			printf("initialize::init_rect.y: %d\n", init_rect.y);
			printf("initialize::init_rect.width: %d\n", init_rect.width);
			printf("initialize::init_rect.height: %d\n", init_rect.height);

			selectObject(cv_img, &init_rect);
			updateCVCorners();
		}
		void update(const cv::Mat &img){
			cv::cvtColor(img, cv_img, CV_BGR2GRAY);
			cv_img = img;
			//printf("img_height: %d\n", cv_img.rows);
			//printf("img_width: %d\n", cv_img.cols);
			update();
		}
		void update(){
			processImage(cv_img);
			updateCVCorners();
		}
		inline void updateCVCorners(){

			//printf("currBB->x: %d\n", currBB->x);
			//printf("currBB->y: %d\n", currBB->y);
			//printf("currBB->height: %d\n", currBB->height);
			//printf("currBB->width: %d\n", currBB->width);

			//printf("currBB: %d\n", currBB);

			double min_x, max_x, min_y, max_y;
			if(currBB){
				min_x = currBB->x;
				max_x = currBB->x + currBB->width;
				min_y = currBB->y;
				max_y = currBB->y + currBB->height;
				cv_corners[0].x = min_x;
				cv_corners[0].y = min_y;
				cv_corners[1].x = max_x;
				cv_corners[1].y = min_y;
				cv_corners[2].x = max_x;
				cv_corners[2].y = max_y;
				cv_corners[3].x = min_x;
				cv_corners[3].y = max_y;
			} else{
				printf("TLD failed !\n");

				cv::Rect *trackerBB = medianFlowTracker->trackerBB;
				int numClusters = detectorCascade->detectionResult->numClusters;
				cv::Rect *detectorBB = detectorCascade->detectionResult->detectorBB;

				if(trackerBB){
					printf("trackerBB->x: %d\n", trackerBB->x);
					printf("trackerBB->y: %d\n", trackerBB->y);
					printf("trackerBB->height: %d\n", trackerBB->height);
					printf("trackerBB->width: %d\n", trackerBB->width);
				} else{
					printf("Median Flow tracker failed !\n");
				}
				if(detectorBB){
					printf("detectorBB->x: %d\n", detectorBB->x);
					printf("detectorBB->y: %d\n", detectorBB->y);
					printf("detectorBB->height: %d\n", detectorBB->height);
					printf("detectorBB->width: %d\n", detectorBB->width);
				} else{
					printf("Detector failed !\n");
				}
				printf("numClusters: %d\n", numClusters);
				//min_x = prevBB->x;
				//max_x = prevBB->x + prevBB->width;
				//min_y = prevBB->y;
				//max_y = prevBB->y + prevBB->height;
			}

			//printf("min_x: %f\n", min_x);
			//printf("max_x: %f\n", max_x);
			//printf("min_y: %f\n", min_y);
			//printf("max_y: %f\n", max_y);
		}
	};

} /* namespace tld */
#endif /* TLD_H_ */

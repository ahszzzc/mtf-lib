# The set of languages for which implicit dependencies are needed:
SET(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
SET(CMAKE_DEPENDS_CHECK_CXX
  "/home/abhineet/E/UofA/Thesis/Code/Tracking Framework/C++/MTF_LIB/ThirdParty/TLD/Clustering.cpp" "/home/abhineet/E/UofA/Thesis/Code/Tracking Framework/C++/MTF_LIB/ThirdParty/TLD/CMakeFiles/libopentld.dir/Clustering.cpp.o"
  "/home/abhineet/E/UofA/Thesis/Code/Tracking Framework/C++/MTF_LIB/ThirdParty/TLD/DetectionResult.cpp" "/home/abhineet/E/UofA/Thesis/Code/Tracking Framework/C++/MTF_LIB/ThirdParty/TLD/CMakeFiles/libopentld.dir/DetectionResult.cpp.o"
  "/home/abhineet/E/UofA/Thesis/Code/Tracking Framework/C++/MTF_LIB/ThirdParty/TLD/DetectorCascade.cpp" "/home/abhineet/E/UofA/Thesis/Code/Tracking Framework/C++/MTF_LIB/ThirdParty/TLD/CMakeFiles/libopentld.dir/DetectorCascade.cpp.o"
  "/home/abhineet/E/UofA/Thesis/Code/Tracking Framework/C++/MTF_LIB/ThirdParty/TLD/EnsembleClassifier.cpp" "/home/abhineet/E/UofA/Thesis/Code/Tracking Framework/C++/MTF_LIB/ThirdParty/TLD/CMakeFiles/libopentld.dir/EnsembleClassifier.cpp.o"
  "/home/abhineet/E/UofA/Thesis/Code/Tracking Framework/C++/MTF_LIB/ThirdParty/TLD/ForegroundDetector.cpp" "/home/abhineet/E/UofA/Thesis/Code/Tracking Framework/C++/MTF_LIB/ThirdParty/TLD/CMakeFiles/libopentld.dir/ForegroundDetector.cpp.o"
  "/home/abhineet/E/UofA/Thesis/Code/Tracking Framework/C++/MTF_LIB/ThirdParty/TLD/MedianFlowTracker.cpp" "/home/abhineet/E/UofA/Thesis/Code/Tracking Framework/C++/MTF_LIB/ThirdParty/TLD/CMakeFiles/libopentld.dir/MedianFlowTracker.cpp.o"
  "/home/abhineet/E/UofA/Thesis/Code/Tracking Framework/C++/MTF_LIB/ThirdParty/TLD/NNClassifier.cpp" "/home/abhineet/E/UofA/Thesis/Code/Tracking Framework/C++/MTF_LIB/ThirdParty/TLD/CMakeFiles/libopentld.dir/NNClassifier.cpp.o"
  "/home/abhineet/E/UofA/Thesis/Code/Tracking Framework/C++/MTF_LIB/ThirdParty/TLD/TLD.cpp" "/home/abhineet/E/UofA/Thesis/Code/Tracking Framework/C++/MTF_LIB/ThirdParty/TLD/CMakeFiles/libopentld.dir/TLD.cpp.o"
  "/home/abhineet/E/UofA/Thesis/Code/Tracking Framework/C++/MTF_LIB/ThirdParty/TLD/TLDUtil.cpp" "/home/abhineet/E/UofA/Thesis/Code/Tracking Framework/C++/MTF_LIB/ThirdParty/TLD/CMakeFiles/libopentld.dir/TLDUtil.cpp.o"
  "/home/abhineet/E/UofA/Thesis/Code/Tracking Framework/C++/MTF_LIB/ThirdParty/TLD/VarianceFilter.cpp" "/home/abhineet/E/UofA/Thesis/Code/Tracking Framework/C++/MTF_LIB/ThirdParty/TLD/CMakeFiles/libopentld.dir/VarianceFilter.cpp.o"
  "/home/abhineet/E/UofA/Thesis/Code/Tracking Framework/C++/MTF_LIB/ThirdParty/TLD/imacq/ImAcq.cpp" "/home/abhineet/E/UofA/Thesis/Code/Tracking Framework/C++/MTF_LIB/ThirdParty/TLD/CMakeFiles/libopentld.dir/imacq/ImAcq.cpp.o"
  "/home/abhineet/E/UofA/Thesis/Code/Tracking Framework/C++/MTF_LIB/ThirdParty/TLD/mftracker/BB.cpp" "/home/abhineet/E/UofA/Thesis/Code/Tracking Framework/C++/MTF_LIB/ThirdParty/TLD/CMakeFiles/libopentld.dir/mftracker/BB.cpp.o"
  "/home/abhineet/E/UofA/Thesis/Code/Tracking Framework/C++/MTF_LIB/ThirdParty/TLD/mftracker/BBPredict.cpp" "/home/abhineet/E/UofA/Thesis/Code/Tracking Framework/C++/MTF_LIB/ThirdParty/TLD/CMakeFiles/libopentld.dir/mftracker/BBPredict.cpp.o"
  "/home/abhineet/E/UofA/Thesis/Code/Tracking Framework/C++/MTF_LIB/ThirdParty/TLD/mftracker/FBTrack.cpp" "/home/abhineet/E/UofA/Thesis/Code/Tracking Framework/C++/MTF_LIB/ThirdParty/TLD/CMakeFiles/libopentld.dir/mftracker/FBTrack.cpp.o"
  "/home/abhineet/E/UofA/Thesis/Code/Tracking Framework/C++/MTF_LIB/ThirdParty/TLD/mftracker/Lk.cpp" "/home/abhineet/E/UofA/Thesis/Code/Tracking Framework/C++/MTF_LIB/ThirdParty/TLD/CMakeFiles/libopentld.dir/mftracker/Lk.cpp.o"
  "/home/abhineet/E/UofA/Thesis/Code/Tracking Framework/C++/MTF_LIB/ThirdParty/TLD/mftracker/Median.cpp" "/home/abhineet/E/UofA/Thesis/Code/Tracking Framework/C++/MTF_LIB/ThirdParty/TLD/CMakeFiles/libopentld.dir/mftracker/Median.cpp.o"
  )
SET(CMAKE_CXX_COMPILER_ID "GNU")

# Targets to which this target links.
SET(CMAKE_TARGET_LINKED_INFO_FILES
  )

# The include file search paths:
SET(CMAKE_C_TARGET_INCLUDE_PATH
  "imacq"
  "mftracker"
  "tld"
  "3rdparty/cvblobs"
  )
SET(CMAKE_CXX_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
SET(CMAKE_Fortran_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
SET(CMAKE_ASM_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})

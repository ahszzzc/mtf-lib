#ifndef CMT_H

#define CMT_H

#include "common.h"
#include "Consensus.h"
#include "Fusion.h"
#include "Matcher.h"
#include "Tracker.h"

#include <opencv2/features2d/features2d.hpp>
#include "opencv2/core/core.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"

#include "../../TrackerBase.h"

#define CMT_RESIZE_FACTOR 0.5

using cv::FeatureDetector;
using cv::DescriptorExtractor;
using cv::Ptr;
using cv::RotatedRect;
using cv::Size2f;


struct CMTParams{
	bool estimate_scale;
	bool estimate_rotation;
	string feat_detector;
	string desc_extractor;
	double resize_factor;

	CMTParams() : estimate_scale(true), estimate_rotation(false),
		feat_detector("FAST"), desc_extractor("BRISK"), resize_factor(CMT_RESIZE_FACTOR){}

	CMTParams(double estimate_scale, double estimate_rotation,
		char* feat_detector, char* desc_extractor, double resize_factor){
		this->estimate_scale = estimate_scale;
		this->estimate_rotation = estimate_rotation;
		this->feat_detector = feat_detector;
		this->desc_extractor = desc_extractor;
		this->resize_factor = resize_factor;
	}

	CMTParams(CMTParams *params) : estimate_scale(true), estimate_rotation(false),
		feat_detector("FAST"), desc_extractor("BRISK"), resize_factor(CMT_RESIZE_FACTOR){
		if(params){
			estimate_scale = params->estimate_scale;
			estimate_rotation = params->estimate_rotation;
			feat_detector = params->feat_detector;
			desc_extractor = params->desc_extractor;
			resize_factor = params->resize_factor;
		}
	}
};

namespace cmt
{

	class CMT : public mtf::TrackerBase
	{
	public:
		typedef CMTParams ParamType;
		ParamType params;
		Mat cv_img;
		Mat cv_img_resized;
		CMT() : TrackerBase(), str_detector("FAST"), str_descriptor("BRISK"){
			name = "cmt";
		}
		CMT(const cv::Mat &img, CMTParams *cmt_params = NULL) : TrackerBase(), params(cmt_params),
			str_detector("FAST"), str_descriptor("BRISK"){
			name = "cmt";
			consensus.estimate_scale = params.estimate_scale;
			consensus.estimate_rotation = params.estimate_rotation;
			str_detector = params.feat_detector;
			str_descriptor = params.desc_extractor;
			printf("Initializing CMT tracker with:\n");
			printf("estimate_scale: %d\n", consensus.estimate_scale);
			printf("estimate_rotation: %d\n", consensus.estimate_rotation);
			printf("str_detector: %s\n", str_detector.c_str());
			printf("str_descriptor: %s\n", str_descriptor.c_str());
			printf("resize_factor: %f\n", params.resize_factor);
			printf("\n");
		};
		bool rgbInput() const { return true; }

		void initialize(const Mat im_gray, const Rect rect);
		void processFrame(const Mat im_gray);

		void initialize(const cv::Mat &img, const cv::Mat &corners){
			cv::cvtColor(img, cv_img, CV_BGR2GRAY);
			cv::resize(cv_img, cv_img_resized, cv::Size(0, 0), params.resize_factor, params.resize_factor);

			//printf("img_height: %d\n", cv_img.rows);
			//printf("img_width: %d\n", cv_img.cols);
			initialize(corners);
		}
		void initialize(const cv::Mat& cv_corners){
			//double pos_x = (cv_corners.at<double>(0, 0) + cv_corners.at<double>(0, 1) +
			//	cv_corners.at<double>(0, 2) + cv_corners.at<double>(0, 3)) / 4;
			//double pos_y = (cv_corners.at<double>(1, 0) + cv_corners.at<double>(1, 1) +
			//	cv_corners.at<double>(1, 2) + cv_corners.at<double>(1, 3)) / 4;

			double pos_x = cv_corners.at<double>(0, 0);
			double pos_y = cv_corners.at<double>(1, 0);
			double size_x = ((cv_corners.at<double>(0, 1) - cv_corners.at<double>(0, 0)) +
				(cv_corners.at<double>(0, 2) - cv_corners.at<double>(0, 3))) / 2;
			double size_y = ((cv_corners.at<double>(1, 3) - cv_corners.at<double>(1, 0)) +
				(cv_corners.at<double>(1, 2) - cv_corners.at<double>(1, 1))) / 2;
			initialize(cv_img_resized, Rect(pos_x*params.resize_factor, pos_y*params.resize_factor,
				size_x*params.resize_factor, size_y*params.resize_factor));
			updateCVCorners();
		}
		void update(const cv::Mat &img){
			cv::cvtColor(img, cv_img, CV_BGR2GRAY);
			cv::resize(cv_img, cv_img_resized, cv::Size(0, 0), params.resize_factor, params.resize_factor);
			//printf("img_height: %d\n", cv_img.rows);
			//printf("img_width: %d\n", cv_img.cols);
			update();
		}
		inline void updateCVCorners(){

			Point2f vertices[4];
			bb_rot.points(vertices);

			for(int i = 0; i < 4; i++){
				cv_corners[i].x = vertices[4-i-1].x / params.resize_factor;
				cv_corners[i].y = vertices[4 - i - 1].y / params.resize_factor;
			}
			//Rect rect = bb_rot.boundingRect();
			//double min_x = rect.x - rect.width / 2.0;
			//double max_x = rect.x + rect.width / 2.0;
			//double min_y = rect.y - rect.height / 2.0;
			//double max_y = rect.y + rect.height / 2.0;
			//cv_corners[0].x = min_x;
			//cv_corners[0].y = min_y;
			//cv_corners[1].x = max_x;
			//cv_corners[1].y = min_y;
			//cv_corners[2].x = max_x;
			//cv_corners[2].y = max_y;
			//cv_corners[3].x = min_x;
			//cv_corners[3].y = max_y;
		}
		void update(){
			processFrame(cv_img_resized);
			updateCVCorners();
		}
		Fusion fusion;
		Matcher matcher;
		Tracker tracker;
		Consensus consensus;

		string str_detector;
		string str_descriptor;

		vector<Point2f> points_active; //public for visualization purposes
		RotatedRect bb_rot;

	private:
		Ptr<FeatureDetector> detector;
		Ptr<DescriptorExtractor> descriptor;

		Size2f size_initial;

		vector<int> classes_active;

		float theta;

		Mat im_prev;
	};

} /* namespace CMT */

#endif /* end of include guard: CMT_H */

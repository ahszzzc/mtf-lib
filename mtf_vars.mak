BASE_CLASSES = AppearanceModel StateSpaceModel SearchMethod TrackerBase mtfMacros mtfRegister
DIAG_BASE_CLASSES = AppearanceModel StateSpaceModel DiagBase mtfMacros mtfRegister

SEARCH_METHODS = FCLK PF NN GNN ESM AESM ICLK FALK IALK FCGD HACLK GridTracker ParallelTracker
APPEARANCE_MODELS = ImageBase SSDBase SSD NSSD ZNCC SCV LSCV RSCV LRSCV CCRE LKLD MI SSIM NCC SPSS KLD
STATE_SPACE_MODELS = ProjectiveBase LieHomography CornerHomography Homography Affine Similitude Isometry Transcaling Translation
UTILITIES = histUtils homUtils imgUtils
UTILITIES_H = miscUtils
COMPOSITE_TRACKERS = CascadeTracker RKLT
_LEARNING_TRACKERS = DSST/DSST KCF/KCFTracker RCT/CompressiveTracker
_LEARNING_TRACKERS_SO = CMT/libcmt.so TLD/libopentld.so 
TOOLS =  parameters datasets inputCV inputBase cvUtils
DIAG_TOOLS = Diagnostics
XVISION_TRACKERS = xvSSDAffine xvSSDGrid xvSSDGridLine xvSSDHelper xvSSDMain xvSSDPyramidAffine xvSSDPyramidRotate xvSSDPyramidRT xvSSDPyramidSE2 xvSSDPyramidTrans xvSSDRotate xvSSDRT xvSSDScaling xvSSDSE2 xvSSDTR xvSSDTrans common xvColor xvEdgeTracker

SEARCH_OBJS := $(addsuffix .o, ${SEARCH_METHODS})
APPEARANCE_OBJS := $(addsuffix .o, ${APPEARANCE_MODELS})
STATE_SPACE_OBJS := $(addsuffix .o, ${STATE_SPACE_MODELS})	
MTF_UTIL_OBJS := $(addsuffix .o, ${UTILITIES})
DIAG_OBJS := $(addsuffix .o, ${DIAG_TOOLS})

BASE_HEADERS = $(addsuffix .h, ${BASE_CLASSES})
DIAG_BASE_HEADERS = $(addsuffix .h, ${DIAG_BASE_CLASSES})
SEARCH_HEADERS = $(addsuffix .h, ${SEARCH_METHODS})
APPEARANCE_HEADERS = $(addsuffix .h, ${APPEARANCE_MODELS})
STATE_SPACE_HEADERS = $(addsuffix .h, ${STATE_SPACE_MODELS})	
MTF_UTIL_HEADERS = $(addsuffix .h, ${UTILITIES})
COMPOSITE_HEADERS = $(addsuffix .h, ${COMPOSITE_TRACKERS})

MTF_UTIL_HEADERS += $(addsuffix .h, ${UTILITIES_H})
TOOL_HEADERS =  $(addprefix Tools/, $(addsuffix .h, ${TOOLS}))
DIAG_HEADERS = Diagnostics.h

LEARNING_TRACKERS =  $(addprefix ThirdParty/, ${_LEARNING_TRACKERS})
LEARNING_TRACKERS_SO =  $(addprefix ThirdParty/, ${_LEARNING_TRACKERS_SO})
LEARNING_OBJS = $(addsuffix .o, ${LEARNING_TRACKERS})
LEARNING_HEADERS = $(addsuffix .h, ${LEARNING_TRACKERS})
XVISION_HEADERS =  $(addprefix ThirdParty/Xvision/, $(addsuffix .h, ${XVISION_TRACKERS}))

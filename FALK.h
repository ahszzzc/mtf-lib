#ifndef FALK_H
#define FALK_H

#include "SearchMethod.h"

#define FALK_MAX_ITERS 10
#define FALK_UPD_THRESH 0.01
#define FALK_HESS_TYPE 0
#define FALK_SECOND_ORDER_HESS false
#define FALK_UPD_TEMPL false
#define FALK_DEBUG_MODE false



_MTF_BEGIN_NAMESPACE

struct FALKParams{
	enum class HessType{ InitialSelf, CurrentSelf, Std };

	int max_iters; //! maximum iterations of the FALK algorithm to run for each frame
	double upd_thresh; //! maximum L1 norm of the state update vector at which to stop the iterations
	HessType hess_type;
	bool sec_ord_hess;
	bool upd_templ;
	bool debug_mode; //! decides whether logging data will be printed for debugging purposes; 
	//! only matters if logging is enabled at compile time

	FALKParams(int _max_iters, double _upd_thresh,
		HessType _hess_type, bool _sec_ord_hess,
		bool _upd_templ, bool _debug_mode){
		max_iters = _max_iters;
		upd_thresh = _upd_thresh;
		hess_type = _hess_type;
		sec_ord_hess = _sec_ord_hess;
		upd_templ = _upd_templ;
		debug_mode = _debug_mode;
	}
	FALKParams(FALKParams *params = nullptr) :
		max_iters(FALK_MAX_ITERS),
		upd_thresh(FALK_UPD_THRESH),
		hess_type(static_cast<HessType>(FALK_HESS_TYPE)),
		sec_ord_hess(FALK_SECOND_ORDER_HESS),
		upd_templ(FALK_UPD_TEMPL),
		debug_mode(FALK_DEBUG_MODE){
		if(params){
			max_iters = params->max_iters;
			upd_thresh = params->upd_thresh;
			hess_type = params->hess_type;
			sec_ord_hess = params->sec_ord_hess;
			upd_templ = params->upd_templ;
			debug_mode = params->debug_mode;
		}
	}
};

template<class AM, class SSM>
class FALK : public SearchMethod < AM, SSM > {

	init_profiling();
	char *log_fname;
	char *time_fname;

public:

	typedef FALKParams ParamType;
	typedef ParamType::HessType HessType;

	ParamType params;

	using SearchMethod<AM, SSM> ::name;
	using SearchMethod<AM, SSM> ::am;
	using SearchMethod<AM, SSM> ::ssm;
	using typename SearchMethod<AM, SSM> ::AMParams;
	using typename SearchMethod<AM, SSM> ::SSMParams;
	using SearchMethod<AM, SSM> ::cv_corners_mat;
	using SearchMethod<AM, SSM> ::cv_corners;
	using SearchMethod<AM, SSM> ::initialize;
	using SearchMethod<AM, SSM> ::update;

	// Let S = size of SSM state vector and N = resx * resy = no. of pixels in the object patch

	//! 1 x S Jacobian of the AM similarity function w.r.t. SSM state vector
	RowVectorXd jacobian;
	//! S x S Hessian of the AM similarity function w.r.t. SSM state vector
	MatrixXd hessian;
	//! N x S jacobians of the pixel values w.r.t the SSM state vector 
	MatrixXd init_pix_jacobian, curr_pix_jacobian;
	//! N x S x S hessians of the pixel values w.r.t the SSM state vector stored as a (S*S) x N 2D matrix
	MatrixXd init_pix_hessian, curr_pix_hessian;

	Matrix24d prev_corners;
	VectorXd ssm_update;
	Matrix3d warp_update;
	int frame_id;

	FALK(ParamType *falk_params = nullptr,
		AMParams *am_params = nullptr, SSMParams *ssm_params = nullptr);

	void initialize(const cv::Mat &corners) override;
	void update() override;
};
_MTF_END_NAMESPACE

#endif


FLAGSCV= `pkg-config --cflags opencv`
FLAGS64= -I/usr/include -I.
WARNING_FLAGS = -Wfatal-errors -Wno-write-strings -Wno-unused-result
FLAGSXV= -I/include/XVision2 -I/usr/include/dc1394/ -I./Xvision
LIBSCV= `pkg-config --libs opencv`
LIBSXV= -L/usr/X11R6/lib -lXVTrack -lXVDevs -lXVCons -lXVSeg -lXVTools \
	-lXVImages -ljpeg -lpng -ltiff -L/usr/X11R6/lib64 -lXext -lX11 -lavformat -lavcodec -lavutil -lpthread -lippi -lippcc -lipps  \
	-lraw1394 -ldc1394 /home/abhineet/XVision2/lib/libmpeg.a /usr/lib/x86_64-linux-gnu/libXxf86dga.so.1 /usr/lib/x86_64-linux-gnu/libXxf86vm.so.1

LIBS_BOOST=  -lboost_random -lboost_filesystem -lboost_system
FLAGS_TBB =  -L/opt/intel/composer_xe_2015/tbb/lib/intel64/gcc4.4
LIBS_PARALLEL = -ltbb
FLAGS_LEARNING =  -L./CMT
LIBS_LEARNING =  -lcmt -lopentld -lcvblobs
LIB_MTF_LIBS =  -L/usr/local/lib -lstdc++ -lgnn  -lflann -lhdf5 -lboost_random



o ?= 1
el ?= 0
et ?= 0
ed ?= 0
emg ?= 1
icd ?= 0
icl ?= 0
ict ?= 0
fcd ?= 0
fct ?= 0
fad ?= 0
fat ?= 0
iat ?= 0
mil ?= 0
mid ?= 0
efd ?= 0
lscd ?= 0
sg ?= 0
pip ?= -1
gip ?= -1
hip ?= -1
nwt ?= 1
tbb ?= 0
spi ?= 1
nnt ?= 0
grtbb ?= 1
ccretbb ?= 0
ccreomp ?= 0
pfsl3 ?= 0
prltbb ?= 1
prlomp ?= 0
lt ?= 1
vp ?= 1
prf ?= 0
xv ?= 0

BUILD_DIR = 
CT_FLAGS = -std=c++11
OPT_FLAGS = 
OPT_FLAGS_DSST = -O2 -msse2 -D NDEBUG
ESM_FLAGS = 
IC_FLAGS =
FC_FLAGS = 
FA_FLAGS = 
SSD_FLAGS = 
TRANS_FLAGS = 
NN_FLAGS = 
AM_FLAGS=
PROF_FLAGS =
RUN_MTF_FLAGS = -std=c++11
LSCV_FLAGS = 
HIST_FLAGS = 
MI_FLAGS = 
CCRE_FLAGS = 
GRID_FLAGS = 
PRL_FLAGS = 

ifeq (${vp}, 1)
VISP_FLAGS = -I /usr/local/include
VISP_LIBS = -L/usr/local/lib/x86_64-linux-gnu -lvisp_core -lvisp_tt -lvisp_tt_mi
_LEARNING_TRACKERS += ViSP/ViSP
endif

ifeq (${vp}, 0)
RUN_MTF_FLAGS += -D DISABLE_VISP
VISP_LIBS =
VISP_FLAGS = 
endif

ifeq (${prltbb}, 1)
PRL_FLAGS += -D ENABLE_TBB
RUN_MTF_FLAGS += -D ENABLE_PARALLEL
endif

ifeq (${mitbb}, 1)
MI_FLAGS += -D ENABLE_TBB
RUN_MTF_FLAGS += -D ENABLE_PARALLEL
endif

ifeq (${miomp}, 1)
MI_FLAGS += -D ENABLE_OMP -fopenmp
RUN_MTF_FLAGS += -D ENABLE_PARALLEL -fopenmp
LIB_MTF_LIBS += -fopenmp
endif

ifeq (${ccretbb}, 1)
CCRE_FLAGS += -D ENABLE_TBB
RUN_MTF_FLAGS += -D ENABLE_PARALLEL
endif

ifeq (${ccreomp}, 1)
CCRE_FLAGS += -D ENABLE_OMP -fopenmp
RUN_MTF_FLAGS += -D ENABLE_PARALLEL -fopenmp
LIB_MTF_LIBS += -fopenmp
endif

ifeq (${pfsl3}, 0)
RUN_MTF_FLAGS += -D DISABLE_PFSL3
endif
ifeq (${pfsl3}, 1)
LEARNING_TRACKERS_SO += -D PFSL3/libpfsl3.a
LIBS_LEARNING += -lpfsl3 -llapacke -llapack -lblas -ltmg -lf2c -lgfortran -lm
endif

ifeq (${lt}, 0)
LEARNING_TRACKERS=
LEARNING_TRACKERS_SO=
LEARNING_OBJS=
LEARNING_HEADERS=
FLAGS_LEARNING=
LIBS_LEARNING=
RUN_MTF_FLAGS += -D DISABLE_LEARNING_TRACKERS
endif
ifeq (${prf}, 1)
PROF_FLAGS += -pg
endif

ifeq (${htbb}, 1)
HIST_FLAGS += -D ENABLE_HIST_TBB
RUN_MTF_FLAGS += -D ENABLE_PARALLEL
endif

ifeq (${lscd}, 1)
LSCV_FLAGS += -D LOG_LSCV_DATA
endif

ifeq (${grtbb}, 1)
GRID_FLAGS += -D ENABLE_TBB
RUN_MTF_FLAGS += -D ENABLE_PARALLEL
endif

ifneq (${pip}, -1)
CT_FLAGS += -D PIX_INTERP_TYPE=${pip}
endif
ifneq (${gip}, -1)
CT_FLAGS += -D GRAD_INTERP_TYPE=${gip}
endif
ifneq (${hip}, -1)
CT_FLAGS += -D HESS_INTERP_TYPE=${hip}
endif
ifeq (${nwt}, 0)
CT_FLAGS += -D DISABLE_NEWTON
endif

ifeq (${xv}, 1)
MTF_FLAGSXV = ${FLAGSXV}
MTF_LIBSXV = ${LIBSXV}
TOOL_HEADERS += inputXV.h
else
XVISION_HEADERS =
MTF_FLAGSXV = -D DISABLE_XVISION
MTF_LIBSXV = 
endif

ifeq (${o}, 1)
OPT_FLAGS = -O3 -D NDEBUG
OPT_FLAGS_MTF = -O3 -msse2 -D NDEBUG 
LIBS_TBB = -ltbb
MTF_LIB_NAME:=libmtf.so
MTF_LIB_LINK:=-lmtf
TOOLS_LIB_NAME:=libmtf_tools.so
TOOLS_LIB_LINK:=-lmtf_tools
MTF_EXE_NAME:=runMTF
MTF_TEST_EXE_NAME:=testMTF
BUILD_DIR = Build/Release
endif

ifeq (${o}, 0)
OPT_FLAGS += -g -O0
OPT_FLAGS_MTF = -g -O0
LIBS_TBB = -ltbb_debug
MTF_LIB_NAME:=libmtf_debug.so
MTF_EXE_NAME:=runMTFd
MTF_LIB_LINK:=-lmtf_debug
TOOLS_LIB_NAME:=libmtf_tools_debug.so
TOOLS_LIB_LINK:=-lmtf_tools_debug
MTF_TEST_EXE_NAME:=testMTFd
BUILD_DIR = Build/Debug
endif

ifeq (${et}, 1)
ESM_FLAGS += -D ENABLE_PROFILING
endif
ifeq (${ed}, 1)
ESM_FLAGS += -D LOG_ESM_DATA
endif
ifeq (${el}, 1)
ESM_FLAGS += -D LOG_ESM_DATA -D ENABLE_PROFILING
endif
ifeq (${emg}, 0)
ESM_FLAGS += -D DISABLE_MEAN_GRADIENT
endif

ifeq (${spi}, 0)
CT_FLAGS += -D DISABLE_SPI
endif

ifeq (${mid}, 1)
MI_FLAGS = -D LOG_MI_DATA
endif
ifeq (${mil}, 1)
MI_FLAGS = -D LOG_MI_DATA -D LOG_MI_TIMES 
endif

ifeq (${icd}, 1)
IC_FLAGS += -D LOG_ICLK_DATA
endif
ifeq (${ict}, 1)
IC_FLAGS += -D LOG_ICLK_TIMES 
endif

ifeq (${fcd}, 1)
FC_FLAGS += -D LOG_FCLK_DATA
endif
ifeq (${fct}, 1)
FC_FLAGS += -D ENABLE_PROFILING 
endif

ifeq (${fad}, 1)
FA_FLAGS += -D LOG_FALK_DATA
endif
ifeq (${fat}, 1)
FA_FLAGS += -D LOG_FALK_TIMES 
endif
ifeq (${iat}, 1)
IA_FLAGS += -D LOG_IALK_TIMES 
endif
ifeq (${nnt}, 1)
NN_FLAGS += -D ENABLE_PROFILING
endif

ifeq (${sg}, 1)
SSD_FLAGS += -D USE_SLOW_GRAD
endif

SEARCH_OBJS:=$(addprefix ${BUILD_DIR}/,${SEARCH_OBJS})
APPEARANCE_OBJS:=$(addprefix ${BUILD_DIR}/,${APPEARANCE_OBJS})
STATE_SPACE_OBJS:=$(addprefix ${BUILD_DIR}/,${STATE_SPACE_OBJS})
MTF_UTIL_OBJS:=$(addprefix ${BUILD_DIR}/,${MTF_UTIL_OBJS})
DIAG_OBJS:=$(addprefix ${BUILD_DIR}/,${DIAG_OBJS})
